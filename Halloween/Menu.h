/*
Syn's AyyWare Framework 2015
*/

#pragma once

#include "GUI.h"
#include "Controls.h"

class CRageBotTab : public CTab
{
public:
	void Setup();

	// Master Switch
	CLabel ActiveLabel;
	CCheckBox Active;

	// Aimbot Settings
	CGroupBox AimbotGroup;
	CCheckBox AimbotEnable;
	CCheckBox AimbotAutoFire;
	CSlider	  AimbotFov;
	CComboBox AimbotSilentAim;
	CCheckBox AimbotPerfectSilentAim;
	CCheckBox AimbotAutoPistol;
	CCheckBox AimbotAutoRevolver;
	CCheckBox AimbotAimStep;
	CCheckBox AimbotKeyPress;
	CKeyBind  AimbotKeyBind;
	CKeyBind  AimbotStopKey;
	CKeyBind  BaimKey;
	CSlider   AimbotMultiPointIndexes;
	CCheckBox AwpAtBody;

	// Target Selection Settings
	CGroupBox TargetGroup;
	CComboBox TargetSelection;
	CCheckBox TargetFriendlyFire;
	CComboBox TargetHitbox;
	CComboBox TargetHitscan;
	CCheckBox TargetMultipoint;
	CComboBox AccuracyDrop;
	CSlider   TargetSmartScan;
	CSlider   TargetPointscale; 

	// Accuracy Settings
	CGroupBox AccuracyGroup;
	CCheckBox AccuracyRecoil;
	CCheckBox AccuracyAutoWall;
	CSlider	  AccuracyMinimumDamage;
	CCheckBox AutoMinimumDamage;
	CCheckBox AutomaticHitchance;
	CCheckBox AccuracyAutoStop;
	CCheckBox AccuracyAutoCrouch;
	CCheckBox AccuracyAutoScope;
	CSlider   AccuracyHitchanceVal;
	CCheckBox AutoHitChance;
	CComboBox AccuracyResolverYaw;
	CCheckBox AccuracyResolverenable;
	CCheckBox lbycorrect;
	CComboBox AccuracyResolverMode;
	CCheckBox AccuracyResolverPitch;
	CCheckBox AccuracyPositionAdjustment;
	CCheckBox AccuracyPrediction;
	CCheckBox AccuracyBacktracking;
};

class CLegitBotTab : public CTab
{
public:
	void Setup();

	// Master Switch
	CLabel ActiveLabel;
	CCheckBox Active;

	// Aimbot Settings
	CGroupBox AimbotGroup;
	CSlider   TickModulation;
	CCheckBox AimbotEnable;
	CCheckBox AimbotAutoFire;
	CCheckBox AimbotFriendlyFire;
	CCheckBox AimbotKeyPress;
	CKeyBind  AimbotKeyBind;
	CSlider   TickDistance;
	CCheckBox AimbotAutoPistol;
	CCheckBox AimbotBacktrack;
	CCheckBox AimbotChicken;
	CCheckBox AimbotSmokeCheck;

	CLabel pistol;
	CLabel rifle;
	CLabel sniper;
	// Main
	CGroupBox TriggerGroup;

	CCheckBox TriggerEnable;
	CCheckBox TriggerKeyPress;
	CKeyBind  TriggerKeyBind;
	CCheckBox TriggerHitChance;
	CSlider   TriggerHitChanceAmmount;
	CSlider   TriggerDelay;
	CSlider   TriggerBurst;
	CSlider   TriggerBreak;
	CCheckBox TriggerRecoil;

	// Trigger Filter
	CGroupBox TriggerFilterGroup;

	CCheckBox TriggerHead;
	CCheckBox TriggerChest;
	CCheckBox TriggerStomach;
	CCheckBox TriggerArms;
	CCheckBox TriggerLegs;
	CCheckBox TriggerTeammates;

	// Rifles
	CGroupBox WeaponMainGroup;

	CSlider   WeaponMainSpeed;
	CSlider   WeaponMainFoV;
	CCheckBox WeaponMainRecoil;
	CCheckBox WeaponMainPSilent;
	CSlider   WeaponMainInacc;
	CComboBox WeaponMainHitbox;
	CSlider WeaponMainAimtime;
	CSlider WeaoponMainStartAimtime;

	// Pistol
	CGroupBox WeaponPistGroup;

	CSlider   WeaponPistSpeed;
	CSlider   WeaponPistFoV;
	CCheckBox WeaponPistRecoil;
	CCheckBox WeaponPistPSilent;
	CSlider   WeaponPistInacc;
	CComboBox WeaponPistHitbox;
	CSlider WeaponPistAimtime;
	CSlider WeaoponPistStartAimtime;

	// Sniper
	CGroupBox WeaponSnipGroup;

	CSlider   WeaponSnipSpeed;
	CSlider   WeaponSnipFoV;
	CCheckBox WeaponSnipRecoil;
	CCheckBox WeaponSnipPSilent;
	CSlider   WeaponSnipInacc;
	CComboBox WeaponSnipHitbox;
	CSlider WeaponSnipAimtime;
	CSlider WeaoponSnipStartAimtime;

	// MPs
	CGroupBox WeaponMpGroup;

	CSlider   WeaponMpSpeed;
	CSlider   WeaponMpFoV;
	CSlider WeaponMpRecoil;
	CCheckBox WeaponMpPSilent;
	CComboBox WeaponMpHitbox;
	CSlider WeaponMpAimtime;
	CSlider WeaoponMpStartAimtime;

	// Shotguns
	CGroupBox WeaponShotgunGroup;

	CSlider   WeaponShotgunSpeed;
	CSlider   WeaponShotgunFoV;
	CSlider WeaponShotgunRecoil;
	CCheckBox WeaponShotgunPSilent;
	CComboBox WeaponShotgunHitbox;
	CSlider WeaponShotgunAimtime;
	CSlider WeaoponShotgunStartAimtime;

	// Machineguns
	CGroupBox WeaponMGGroup;

	CSlider   WeaponMGSpeed;
	CSlider   WeaponMGFoV;
	CSlider WeaponMGRecoil;
	CCheckBox WeaponMGPSilent;
	CComboBox WeaponMGHitbox;
	CSlider WeaponMGAimtime;
	CSlider WeaoponMGStartAimtime;
};

class CVisualTab : public CTab
{
public:
	void Setup();

	// Master Switch
	CLabel ActiveLabel;
	CCheckBox Active;

	// Options Settings
	CGroupBox OptionsGroup;
	CCheckBox OptionsBox;
	CCheckBox OptionsFillBox;
	CCheckBox OptionsName;
	CComboBox OptionsVitals;
	CCheckBox OptionsBarrel;
	CCheckBox OptionsWeapon;
	CCheckBox OptionsInfo;
	CCheckBox OptionsHelmet;
	CCheckBox OtherSniperCrosshair;
	CCheckBox OtherSpreadCrosshair;
	CCheckBox OptionsKit;
	CCheckBox OptionsDefuse;
	CCheckBox OptionsGlow;
	CComboBox OptionsChams;
	CCheckBox OptionsSkeleton;
	CCheckBox OptionsAimSpot;
	CCheckBox OptionsCompRank;
	CCheckBox ChamsXQZ;
	CCheckBox GrenadeTracer;
	CCheckBox ShowImpacts;
	CCheckBox IsScoped;
	CCheckBox HasDefuser;
	CCheckBox IsDefusing;
	CCheckBox BacktrackingLol;
	CCheckBox RemoveScope;
	CCheckBox OptionsLBY;
	CCheckBox OtherGlow;
	CCheckBox Grenades;
	CSlider   BulletTraceLength;

	// Filters Settings
	CGroupBox FiltersGroup;
	CCheckBox FiltersAll;
	CCheckBox FiltersPlayers;
	CCheckBox FiltersEnemiesOnly;
	CCheckBox FiltersWeapons;
	CCheckBox FiltersChickens;
	CCheckBox FiltersC4;

	// Other Settings
	CGroupBox OtherGroup;
	CCheckBox OtherCrosshair;
	CComboBox OtherRecoilCrosshair;
	CComboBox OtherHitmarkerType;
	CCheckBox OtherHitmarker;
	CCheckBox OtherRadar;
	CCheckBox OtherNoVisualRecoil;
	CCheckBox OtherNoSky; 
	CCheckBox OtherNoFlash; 
	CCheckBox OtherPlayerLine;
	CCheckBox LbyIndic;
	CCheckBox OtherEnemyCircle;
	CCheckBox OtherNoSmoke;
	CCheckBox OtherAsusWalls;
	CComboBox OtherNoHands;
	CCheckBox HandXQZ;
	CSlider OtherViewmodelFOV;
	CSlider OtherFOV;
	CColorSlider Test;

	// Cvar Options
	CGroupBox CvarGroup;
	CSlider AmbientExposure;
	CSlider AmbientRed;
	CSlider AmbientGreen;
	CSlider AmbientBlue;
	CComboBox AmbientSkybox;
	CCheckBox NightMode;
};

class CMiscTab : public CTab
{
public:
	void Setup();

	// Knife Changer
	CGroupBox KnifeGroup;
	CCheckBox KnifeEnable;
	CComboBox KnifeModel;
	CComboBox KnifeSkin;
	CButton   KnifeApply;

	// Other Settings
	CGroupBox OtherGroup;
	CCheckBox OtherAutoJump;
	CCheckBox OtherEdgeJump;
	CComboBox OtherAutoStrafe;
	CCheckBox OtherSafeMode;
	CComboBox OtherHitmarkerType;
	CComboBox OtherChatSpam;
	CCheckBox OtherTeamChat;
	CSlider	  OtherChatDelay;
	CKeyBind  OtherAirStuck;
	CKeyBind  OtherLagSwitch;
	CCheckBox OtherSpectators;
	CCheckBox OtherCrasher;
	CCheckBox OtherHitmarker;
	CCheckBox OtherMemewalk;
	CComboBox FakeLagTyp;
	CCheckBox memespam;
	CCheckBox legend;

	// Fake Lag Settings
	CGroupBox FakeLagGroup;
	CCheckBox FakeLagEnable;
	CSlider   FakeLagChoke;
	//CCheckBox FakeLagWhileShooting;

	CCheckBox OtherThirdperson;
	CComboBox OtherThirdpersonType;
	CKeyBind OtherThirdpersonKey;
	CKeyBind bindkey2;
	CKeyBind bindkey3;
	//CCheckBox OtherAutoAccept;
	CCheckBox OtherWalkbot;
	CCheckBox OtherClantag; 


	// Fake Lag Settings
	CKeyBind FakeWalk;

	//CCheckBox FakeLagWhileShooting;

	// Teleport shit cause we're cool
	CGroupBox TeleportGroup;
	CCheckBox TeleportEnable;
	CKeyBind  TeleportKey;
	
};

class CAntiAimTab : public CTab
{
	public:
		void Setup();

		//MASTER SWITCH
		CLabel AntiAimActiveLabel;
		CCheckBox AntiAimActive;

		// Anti-Aim Settings
		CGroupBox AntiAimGroup;
		CGroupBox MiscAA;
		CGroupBox OthersAA;
		CCheckBox AntiAimEnable;
		CComboBox AntiAimPitch;
		CComboBox staticyaw;
		CComboBox movingyaw;
		CComboBox fakeyaw;
		CComboBox moveyaw;
		CComboBox fakemove;
		CComboBox edgeyaw;
		CComboBox AntiAimEdge;
		CSlider	  AntiAimOffset;
		CSlider	  SpinSpeed;
		CSlider	  SpinSpeed2;
		CSlider	  AntiAimOffsetReal;
		CSlider	  AntiAimOffsetFake;
		CCheckBox AntiAimKnife;
		CCheckBox NoPlayers;
		CCheckBox NoSpreadServer;
		CCheckBox AntiAimTarget;  
		CCheckBox BreakLBY;
		CCheckBox AntiAimJitterLBY; 
		CSlider	  AntiAimSpinspeed;
		CComboBox AntiResolver;
		CKeyBind  FlipKey;
};

class CColorTab : public CTab
{
	public:
		void Setup();

		CGroupBox ColorGroup;
		CGroupBox CTColorGroup;
		CGroupBox TerroristColorGroup;
		CLabel  PlayerEsp;
		

		//FillBox
		CSlider	  FillBoxRed;
		CSlider	  FillBoxGreen;
		CSlider	  FillBoxBlue;
		CSlider	  FillBoxAlpha;
	
		//Box esp visble
		CSlider	  VisBoxRed;
		CSlider	  VisBoxGreen;
		CSlider	  VisBoxBlue;
		CSlider	  VisBoxAlpha;

		CSlider	  VisBoxRed2;
		CSlider	  VisBoxGreen2;
		CSlider	  VisBoxBlue2;
		CSlider	  VisBoxAlpha2;

		//Box esp 
		CSlider	  BoxRed;
		CSlider	  BoxGreen;
		CSlider	  BoxBlue;
		CSlider	  BoxAlpha;

		CSlider	  BoxRed2;
		CSlider	  BoxGreen2;
		CSlider	  BoxBlue2;
		CSlider	  BoxAlpha2;

		//Chams 
		CSlider	  CTChamsRed;
		CSlider	  CTChamsGreen;
		CSlider	  CTChamsBlue;

		CSlider	  CTVisChamsRed;
		CSlider	  CTVisChamsGreen;
		CSlider	  CTVisChamsBlue;

		CSlider	 TerroristChamsRed;
		CSlider	 TerroristChamsGreen;
		CSlider	TerroristChamsBlue;

		CSlider	 TerroristVisChamsRed;
		CSlider	 TerroristVisChamsGreen;
		CSlider	TerroristVisChamsBlue;



};



class SkinTab : public CTab
{
public:
	void Setup();

	// Knife Changer/Skin Changer
	CLabel SkinActive;
	CCheckBox SkinEnable;
	CButton   SkinApply;

	// Knife
	CGroupBox KnifeGroup;
	CComboBox KnifeModel;
	CComboBox KnifeSkin;

	// Pistols
	CGroupBox PistolGroup;
	CComboBox GLOCKSkin;
	CComboBox USPSSkin;
	CComboBox DEAGLESkin;
	CComboBox MAGNUMSkin;
	CComboBox DUALSSkin;
	CComboBox FIVESEVENSkin;
	CComboBox TECNINESkin;
	CComboBox P2000Skin;
	CComboBox P250Skin;

	// MPs
	CGroupBox MPGroup;
	CComboBox MAC10Skin;
	CComboBox P90Skin;
	CComboBox UMP45Skin;
	CComboBox BIZONSkin;
	CComboBox MP7Skin;
	CComboBox MP9Skin;

	// Rifles
	CGroupBox Riflegroup;
	CComboBox M41SSkin;
	CComboBox M4A4Skin;
	CComboBox AK47Skin;
	CComboBox AUGSkin;
	CComboBox FAMASSkin;
	CComboBox GALILSkin;
	CComboBox SG553Skin;


	// Machineguns
	CGroupBox MachinegunsGroup;
	CComboBox NEGEVSkin;
	CComboBox M249Skin;

	// Snipers
	CGroupBox Snipergroup;
	CComboBox SCAR20Skin;
	CComboBox G3SG1Skin;
	CComboBox SSG08Skin;
	CComboBox AWPSkin;

	// Shotguns
	CGroupBox Shotgungroup;
	CComboBox MAG7Skin;
	CComboBox XM1014Skin;
	CComboBox SAWEDOFFSkin;
	CComboBox NOVASkin;
};

class SkinTab2 : public CTab
{
public:
	void Setup();

	// Knife Changer/Skin Changer
	CLabel SkinActive;
	CCheckBox SkinEnable;
	CButton   SkinApply;

	// Knife
	CGroupBox KnifeGroup;
	CComboBox KnifeModel;
	CComboBox KnifeSkin;

	// Pistols
	CGroupBox PistolGroup;
	CComboBox GLOCKSkin;
	CComboBox USPSSkin;
	CComboBox DEAGLESkin;
	CComboBox MAGNUMSkin;
	CComboBox DUALSSkin;
	CComboBox FIVESEVENSkin;
	CComboBox TECNINESkin;
	CComboBox P2000Skin;
	CComboBox P250Skin;

	// MPs
	CGroupBox MPGroup;
	CComboBox MAC10Skin;
	CComboBox P90Skin;
	CComboBox UMP45Skin;
	CComboBox BIZONSkin;
	CComboBox MP7Skin;
	CComboBox MP9Skin;

	// Rifles
	CGroupBox Riflegroup;
	CComboBox M41SSkin;
	CComboBox M4A4Skin;
	CComboBox AK47Skin;
	CComboBox AUGSkin;
	CComboBox FAMASSkin;
	CComboBox GALILSkin;
	CComboBox SG553Skin;


	// Machineguns
	CGroupBox MachinegunsGroup;
	CComboBox NEGEVSkin;
	CComboBox M249Skin;

	// Snipers
	CGroupBox Snipergroup;
	CComboBox SCAR20Skin;
	CComboBox G3SG1Skin;
	CComboBox SSG08Skin;
	CComboBox AWPSkin;

	// Shotguns
	CGroupBox Shotgungroup;
	CComboBox MAG7Skin;
	CComboBox XM1014Skin;
	CComboBox SAWEDOFFSkin;
	CComboBox NOVASkin;
};



/*class CPlayersTab : public CTab
{
public:
	void Setup();

	CGroupBox pListGroup;

	CGroupBox pListPlayers;

	CGroupBox OptionsGroup;

	CSlider OptionsFriendly;

	CSlider OptionsAimPrio;

	CSlider OptionsCalloutSpam;

	CLabel SetColumns;

};*/

class AyyWareWindow : public CWindow
{
public:
	void Setup();

	CRageBotTab RageBotTab;
	CLegitBotTab LegitBotTab;
	CVisualTab VisualsTab;
	CMiscTab MiscTab;
	CAntiAimTab AntiAimTab;
	CColorTab ColorTab;
	SkinTab SkinTab;
	SkinTab2 SkinTab2;
	//CPlayersTab Playerlist;

	CButton SaveButton;
	CButton LoadButton;
	CButton UnloadButton;

	CComboBox ConfigBox;
};

namespace Menu
{
	void SetupMenu();
	void DoUIFrame();

	extern AyyWareWindow Window;
};