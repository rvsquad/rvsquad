/*
Syn's AyyWare Framework
*/

#include "MetaInfo.h"
#include "Utilities.h"

void PrintMetaHeader()
{
	printf("                                  rv");
	Utilities::SetConsoleColor(FOREGROUND_INTENSE_RED);
	printf("squad");
	Utilities::SetConsoleColor(FOREGROUND_INTENSE_WHITE);
	printf(".");
	Utilities::SetConsoleColor(FOREGROUND_MAGENTA);
	printf("cc\n");
	//Utilities::SetConsoleColor(FOREGROUND_WHITE);
	//Utilities::Log("Build %s", __DATE__);
	//Utilities::Log("rvsquad for %s", META_GAME);
}