/*
Syn's AyyWare Framework 2015
*/

#include "Menu.h"
#include "Controls.h"
#include "Hooks.h" // for the unload meme
#include "Interfaces.h"
#include "CRC32.h"
#include "imgui sdk\imgui.h"

#define WINDOW_WIDTH 850
#define WINDOW_HEIGHT 650

AyyWareWindow Menu::Window;

void SaveCallbk()
{
	switch (Menu::Window.ConfigBox.GetIndex())
	{
	case 0:
		GUI.SaveWindowState(&Menu::Window, "rage.cfg");
		break;
	case 1:
		GUI.SaveWindowState(&Menu::Window, "legit.cfg");
		break;
	case 2:
		GUI.SaveWindowState(&Menu::Window, "other.cfg");
		break;
	case 3:
		GUI.SaveWindowState(&Menu::Window, "mmhvh1.cfg");
		break;
	case 4:
		GUI.SaveWindowState(&Menu::Window, "mmhvh2.cfg");
		break;

	}
}

void LoadCallbk()
{
	switch (Menu::Window.ConfigBox.GetIndex())
	{
	case 0:
		GUI.LoadWindowState(&Menu::Window, "rage.cfg");
		break;
	case 1:
		GUI.LoadWindowState(&Menu::Window, "legit.cfg");
		break;
	case 2:
		GUI.LoadWindowState(&Menu::Window, "other.cfg");
		break;
	case 3:
		GUI.LoadWindowState(&Menu::Window, "mmhvh1.cfg");
		break;
	case 4:
		GUI.LoadWindowState(&Menu::Window, "mmhvh2.cfg");
		break;
	}
}


void UnLoadCallbk()
{
	DoUnload = true;
}

void KnifeApplyCallbk()
{
	static ConVar* Meme = Interfaces::CVar->FindVar("cl_fullupdate");
	Meme->nFlags &= ~FCVAR_CHEAT;
	Interfaces::Engine->ClientCmd_Unrestricted("cl_fullupdate");
}

void AyyWareWindow::Setup()
{
	SetPosition(50, 50);
	SetSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	SetTitle("rvsquad");

	RegisterTab(&RageBotTab);
	RegisterTab(&LegitBotTab);
	RegisterTab(&VisualsTab);
	RegisterTab(&MiscTab);
	RegisterTab(&AntiAimTab);
//	RegisterTab(&ColorTab);
	//RegisterTab(&SkinTab);
	//RegisterTab(&Playerlist);

	RECT Client = GetClientArea();
	Client.bottom -= 29;

	RageBotTab.Setup();
	LegitBotTab.Setup();
	VisualsTab.Setup();
	MiscTab.Setup();
	AntiAimTab.Setup();
//	ColorTab.Setup();
	//SkinTab.Setup();
	//Playerlist.Setup();


	ConfigBox.SetFileId("cfg_box");
	ConfigBox.AddItem("rage");
	ConfigBox.AddItem("legit");
	ConfigBox.AddItem("other");
	ConfigBox.AddItem("mmhvh1");
	ConfigBox.AddItem("mmhvh2");
	ConfigBox.SetPosition(32, Client.bottom - 195);
	ConfigBox.SetSize(95, 200);

	SaveButton.SetText("save");
	SaveButton.SetCallback(SaveCallbk);
	SaveButton.SetSize(95, 200);
	SaveButton.SetPosition(135, Client.bottom - 200);

	LoadButton.SetText("load");
	LoadButton.SetCallback(LoadCallbk);
	LoadButton.SetSize(95, 200);
	LoadButton.SetPosition(235, Client.bottom - 200);


	MiscTab.RegisterControl(&SaveButton);
	MiscTab.RegisterControl(&LoadButton);
	MiscTab.RegisterControl(&ConfigBox);

}

void CRageBotTab::Setup()
{
	SetTitle("a");

	ActiveLabel.SetPosition(32, 48);
	ActiveLabel.SetText("active");
	RegisterControl(&ActiveLabel);

	Active.SetFileId("active");
	Active.SetPosition(78, 48);
	RegisterControl(&Active);

#pragma region Aimbot

	AimbotGroup.SetPosition(16, 42);
	AimbotGroup.SetText("aimbot");
	AimbotGroup.SetSize(376, 360);
	RegisterControl(&AimbotGroup);
	AimbotEnable.SetFileId("aim_enable");
	AimbotGroup.PlaceLabledControl("enable", this, &AimbotEnable);

	AimbotAutoFire.SetFileId("aim_autofire");
	AimbotAutoFire.SetState(true);
	//AimbotGroup.PlaceLabledControl("", this, &AimbotAutoFire);

	AimbotFov.SetFileId("aim_fov");
	//AimbotFov.SetBoundaries(0.f, 360.f);
	AimbotFov.SetValue(180.f);
	//AimbotGroup.PlaceLabledControl("aimbot fov", this, &AimbotFov);

	AimbotSilentAim.SetFileId("aim_silentmethod");
	AimbotSilentAim.AddItem("lock");
	AimbotSilentAim.AddItem("client");
	AimbotSilentAim.AddItem("server");
	AimbotGroup.PlaceLabledControl("method", this, &AimbotSilentAim);

	/*AimbotPerfectSilentAim.SetFileId("aim_psilent");
	AimbotGroup.PlaceLabledControl("Perfect Silent", this, &AimbotPerfectSilentAim);*/

	AimbotAutoPistol.SetFileId("aim_autopistol");
	AimbotGroup.PlaceLabledControl("auto pistol", this, &AimbotAutoPistol);

	AimbotAimStep.SetFileId("aim_aimstep");
	AimbotGroup.PlaceLabledControl("limit angles", this, &AimbotAimStep);

	AimbotKeyPress.SetFileId("aim_usekey");
	AimbotGroup.PlaceLabledControl("on key", this, &AimbotKeyPress);

	AimbotKeyBind.SetFileId("aim_key");
	AimbotGroup.PlaceLabledControl("key", this, &AimbotKeyBind);

	AimbotAutoRevolver.SetFileId("aim_acock");
	AimbotGroup.PlaceLabledControl("automatic Revolver", this, &AimbotAutoRevolver);

	//TargetGroup.SetPosition(16, 262);
	//TargetGroup.SetText("target");
	//TargetGroup.SetSize(376, 198);
	//RegisterControl(&TargetGroup);

	TargetSelection.SetFileId("tgt_selection");
	TargetSelection.AddItem("thru");
	TargetSelection.AddItem("rowerek");
	TargetSelection.AddItem("hp");
	AimbotGroup.PlaceLabledControl("detection", this, &TargetSelection);

	//TargetFriendlyFire.SetFileId("tgt_friendlyfire");
	//TargetGroup.PlaceLabledControl("Shoot Teammates", this, &TargetFriendlyFire);

	TargetHitbox.SetFileId("tgt_hitbox");
	TargetHitbox.AddItem("head");
	TargetHitbox.AddItem("chest");
	AimbotGroup.PlaceLabledControl("spot", this, &TargetHitbox);

	TargetHitscan.SetFileId("tgt_hitscan");
	TargetHitscan.AddItem("off"); /*Disabled*/
	TargetHitscan.AddItem("normal");
	TargetHitscan.AddItem("max");
	AimbotGroup.PlaceLabledControl("scan", this, &TargetHitscan);

	AwpAtBody.SetFileId("acc_bodyawp");
	AimbotGroup.PlaceLabledControl("snipe body", this, &AwpAtBody);

//	TargetMultipoint.SetFileId("tgt_multipoint");
//	TargetGroup.PlaceLabledControl("Multipoint", this, &TargetMultipoint);

	TargetPointscale.SetFileId("aim_mpindexes");
	TargetPointscale.SetBoundaries(0.f, 1.f);
	TargetPointscale.SetValue(0.f);
	AimbotGroup.PlaceLabledControl("aim-scale", this, &TargetPointscale);

//	TargetPointscale.SetFileId("acc_pointscale");
//	TargetPointscale.SetBoundaries(0.f, 1.0f);
//	TargetPointscale.SetValue(0.0f);
//	TargetGroup.PlaceLabledControl("Pointscale", this, &TargetPointscale);

/*	TargetMultipoint.SetFileId("tgt_multipoint");
	TargetGroup.PlaceLabledControl("Multipoint", this, &TargetMultipoint);

	TargetPointscale.SetFileId("tgt_pointscale");
	TargetPointscale.SetBoundaries(0.1f, 10.f);
	TargetPointscale.SetValue(5.f);
	TargetGroup.PlaceLabledControl("Hitbox Scale", this, &TargetPointscale);*/
#pragma endregion Aimbot Controls Get Setup in here

#pragma region Accuracy
	AccuracyGroup.SetPosition(408, 48);
	AccuracyGroup.SetText("accuracy");
	AccuracyGroup.SetSize(360, 360); //280
	RegisterControl(&AccuracyGroup);

	AccuracyRecoil.SetFileId("acc_norecoil");
	AccuracyGroup.PlaceLabledControl("no-recoil", this, &AccuracyRecoil);

	AccuracyAutoWall.SetFileId("acc_awall");
	AccuracyGroup.PlaceLabledControl("thru walls", this, &AccuracyAutoWall);

	AccuracyMinimumDamage.SetFileId("acc_mindmg");
	AccuracyMinimumDamage.SetBoundaries(1.f, 99.f);
	AccuracyMinimumDamage.SetValue(1.f);
	AccuracyGroup.PlaceLabledControl("minimum damage", this, &AccuracyMinimumDamage);

	AutoMinimumDamage.SetFileId("acc_AutoMinimumDamage");
	AccuracyGroup.PlaceLabledControl("dynamic damage", this, &AutoMinimumDamage);

//	AccuracyAutoStop.SetFileId("acc_stop");
//	AccuracyGroup.PlaceLabledControl("Quick Stop", this, &AccuracyAutoStop);

	AccuracyAutoScope.SetFileId("acc_scope");
	AccuracyGroup.PlaceLabledControl("auto scope", this, &AccuracyAutoScope);//

	AccuracyDrop.SetFileId("acc_accuracydrop");
	AccuracyDrop.AddItem("hit-chance");
	AccuracyDrop.AddItem("limit spread");
	AccuracyGroup.PlaceLabledControl("accuracy help", this, &AccuracyDrop);

	AccuracyHitchanceVal.SetFileId("acc_chance");
	AccuracyHitchanceVal.SetBoundaries(0, 100);
	AccuracyHitchanceVal.SetValue(0);
	AccuracyGroup.PlaceLabledControl("min. accuracy help", this, &AccuracyHitchanceVal);

	AutomaticHitchance.SetFileId("acc_autohitchance");
	AccuracyGroup.PlaceLabledControl("dynamic help", this, &AutomaticHitchance);

	//AutoHitChance.SetFileId("acc_AutoHitChance");
	//AccuracyGroup.PlaceLabledControl("Dynamic Hitchance", this, &AutoHitChance);

	/*AccuracyResolver.SetFileId("acc_aaa");
	AccuracyResolver.AddItem("Off");
	AccuracyResolver.AddItem("on");
	AccuracyResolver.AddItem("Pitch resolver");
	AccuracyGroup.PlaceLabledControl("Anti Aim Resolver", this, &AccuracyResolver);*/

	lbycorrect.SetFileId("acc_lbyfix");
	AccuracyGroup.PlaceLabledControl("lby correct/fix", this, &lbycorrect);

	AccuracyResolverenable.SetFileId("acc_reslvrenbl");
	AccuracyGroup.PlaceLabledControl("correction angle", this, &AccuracyResolverenable);

	AccuracyResolverMode.SetFileId("acc_resolvermodexd");
	AccuracyResolverMode.AddItem("brute");
	AccuracyResolverMode.AddItem("basic");
	AccuracyGroup.PlaceLabledControl("correction mode", this, &AccuracyResolverMode);

	//BaimKey.SetFileId("acc_baimlul");
	//AccuracyGroup.PlaceLabledControl("Strict Body Key", this, &BaimKey);

//	AccuracyResolverYaw.SetFileId("acc_yawresolver");
//	AccuracyResolverYaw.AddItem("Lowerbody");
//	AccuracyResolverYaw.AddItem("Server Calc");
//	AccuracyResolverYaw.AddItem("Static Calc");
//	AccuracyResolverYaw.AddItem("Angle Step");
//	AccuracyResolverYaw.AddItem("Animstate");
//	AccuracyGroup.PlaceLabledControl("Resolver Mode", this, &AccuracyResolverYaw);

	//	AccuracyResolverPitch.SetFileId("acc_pitadj");
	//	AccuracyGroup.PlaceLabledControl("Pitch Resolver", this, &AccuracyResolverPitch);

	AccuracyPositionAdjustment.SetFileId("acc_posadj");
	AccuracyGroup.PlaceLabledControl("interp fix", this, &AccuracyPositionAdjustment);

	AccuracyPrediction.SetFileId("acc_engnprd");
	AccuracyGroup.PlaceLabledControl("engine pred", this, &AccuracyPrediction);

	AccuracyBacktracking.SetFileId("acc_bcktrk");
	AccuracyGroup.PlaceLabledControl("tickrate fix", this, &AccuracyBacktracking);

#pragma endregion  Accuracy controls get Setup in here
}

void CLegitBotTab::Setup()
{
	SetTitle("b");

	ActiveLabel.SetPosition(32, 45);
	ActiveLabel.SetText("Active");
	RegisterControl(&ActiveLabel);

	Active.SetFileId("active");
	Active.SetPosition(78, 48);
	RegisterControl(&Active);

#pragma region Aimbot
	AimbotGroup.SetPosition(16, 48);
	AimbotGroup.SetText("Aimbot");
	AimbotGroup.SetSize(376, 250);
	RegisterControl(&AimbotGroup);

	AimbotEnable.SetFileId("aim_enable");
	AimbotGroup.PlaceLabledControl("Legitbot", this, &AimbotEnable);

//	AimbotAutoFire.SetFileId("aim_autofire");
//	AimbotGroup.PlaceLabledControl("Automatic Shoot", this, &AimbotAutoFire);

	AimbotAutoPistol.SetFileId("aim_apistol");
	AimbotGroup.PlaceLabledControl("Automatatic Pistol", this, &AimbotAutoPistol);

	AimbotBacktrack.SetFileId("legit_backtrack");
	AimbotGroup.PlaceLabledControl("Backtrack Ticks", this, &AimbotBacktrack);

	TickModulation.SetFileId("tick_modulate");
	TickModulation.SetBoundaries(0.1f, 13.f);
	TickModulation.SetValue(13.f);
	AimbotGroup.PlaceLabledControl("Tick Modulation", this, &TickModulation);
//	AimbotKeyPress.SetFileId("aim_usekey");
//	AimbotGroup.PlaceLabledControl("Backtrack Wait", this, &AimbotKeyPress);

//	AimbotKeyBind.SetFileId("aim_key");
//	AimbotGroup.PlaceLabledControl("Wait Key", this, &AimbotKeyBind);

//	AimbotFriendlyFire.SetFileId("aim_friendfire");
//	AimbotGroup.PlaceLabledControl("Friendly Fire", this, &AimbotFriendlyFire);

#pragma endregion Aimbot shit

#pragma region Triggerbot
/*	TriggerGroup.SetPosition(16, 168);
	TriggerGroup.SetText("Triggerbot");
	TriggerGroup.SetSize(376, 250);
	RegisterControl(&TriggerGroup);

	TriggerEnable.SetFileId("trig_enable");
	TriggerGroup.PlaceLabledControl("Triggerbot", this, &TriggerEnable);

	TriggerKeyPress.SetFileId("trig_onkey");
	TriggerGroup.PlaceLabledControl("Triggetbot Wait", this, &TriggerKeyPress);

	TriggerKeyBind.SetFileId("trig_key");
	TriggerGroup.PlaceLabledControl("Wait Key", this, &TriggerKeyBind);

	TriggerDelay.SetFileId("trig_time");
	TriggerDelay.SetBoundaries(0.f, 1000.f);
	TriggerGroup.PlaceLabledControl("Millisecond Delay", this, &TriggerDelay);
#pragma endregion Triggerbot stuff*/

#pragma region Main Weapon
/*	WeaponMainGroup.SetPosition(16, 274);
	WeaponMainGroup.SetText("Rifles/Other");
	WeaponMainGroup.SetSize(240, 210);
	RegisterControl(&WeaponMainGroup);

	WeaponMainSpeed.SetFileId("main_speed");
	WeaponMainSpeed.SetBoundaries(0.1f, 2.f);
	WeaponMainSpeed.SetValue(1.0f);
	WeaponMainGroup.PlaceLabledControl("Maximum Speed", this, &WeaponMainSpeed);

	WeaponMainFoV.SetFileId("main_fov");
	WeaponMainFoV.SetBoundaries(0.1f, 30.f);
	WeaponMainFoV.SetValue(5.f);
	WeaponMainGroup.PlaceLabledControl("Maximum FOV", this, &WeaponMainFoV);

	WeaponMainRecoil.SetFileId("main_recoil");
	WeaponMainGroup.PlaceLabledControl("Ajust Recoil", this, &WeaponMainRecoil);

	WeaponMainPSilent.SetFileId("main_psilent");
	WeaponMainGroup.PlaceLabledControl("Silent Aim", this, &WeaponMainPSilent);

	WeaponMainInacc.SetFileId("main_inacc");
	WeaponMainInacc.SetBoundaries(0.f, 15.f);
	WeaponMainGroup.PlaceLabledControl("Miss Chance", this, &WeaponMainInacc);

	WeaponMainHitbox.SetFileId("main_hitbox");
	WeaponMainHitbox.AddItem("Head");
	WeaponMainHitbox.AddItem("Neck");
	WeaponMainHitbox.AddItem("Chest");
	WeaponMainHitbox.AddItem("Stomach");
	WeaponMainHitbox.AddItem("Nearest");
	WeaponMainGroup.PlaceLabledControl("Hitbox", this, &WeaponMainHitbox);
#pragma endregion

#pragma region Pistols
	WeaponPistGroup.SetPosition(272, 274);
	WeaponPistGroup.SetText("Pistols");
	WeaponPistGroup.SetSize(240, 210);
	RegisterControl(&WeaponPistGroup);

	WeaponPistSpeed.SetFileId("pist_speed");
	WeaponPistSpeed.SetBoundaries(0.1f, 2.f);
	WeaponPistSpeed.SetValue(1.0f);
	WeaponPistGroup.PlaceLabledControl("Maximum Speed", this, &WeaponPistSpeed);

	WeaponPistFoV.SetFileId("pist_fov");
	WeaponPistFoV.SetBoundaries(0.1f, 30.f);
	WeaponPistFoV.SetValue(5.f);
	WeaponPistGroup.PlaceLabledControl("Maximum FOV", this, &WeaponPistFoV);

	WeaponPistRecoil.SetFileId("pist_recoil");
	WeaponPistGroup.PlaceLabledControl("Ajust Recoil", this, &WeaponPistRecoil);

	WeaponPistPSilent.SetFileId("pist_psilent");
	WeaponPistGroup.PlaceLabledControl("Silent Aim", this, &WeaponPistPSilent);

	WeaponPistInacc.SetFileId("pist_inacc");
	WeaponPistInacc.SetBoundaries(0.f, 15.f);
	WeaponPistGroup.PlaceLabledControl("Miss Chance", this, &WeaponPistInacc);

	WeaponPistHitbox.SetFileId("pist_hitbox");
	WeaponPistHitbox.AddItem("Head");
	WeaponPistHitbox.AddItem("Neck");
	WeaponPistHitbox.AddItem("Chest");
	WeaponPistHitbox.AddItem("Stomach");
	WeaponPistHitbox.AddItem("Nearest");
	WeaponPistGroup.PlaceLabledControl("Hitbox", this, &WeaponPistHitbox);
#pragma endregion

#pragma region Snipers
	WeaponSnipGroup.SetPosition(528, 274);
	WeaponSnipGroup.SetText("Snipers");
	WeaponSnipGroup.SetSize(240, 210);
	RegisterControl(&WeaponSnipGroup);

	WeaponSnipSpeed.SetFileId("snip_speed");
	WeaponSnipSpeed.SetBoundaries(0.1f, 2.f);
	WeaponSnipSpeed.SetValue(1.0f);
	WeaponSnipGroup.PlaceLabledControl("Maximum Speed", this, &WeaponSnipSpeed);

	WeaponSnipFoV.SetFileId("snip_fov");
	WeaponSnipFoV.SetBoundaries(0.1f, 30.f);
	WeaponSnipFoV.SetValue(5.f);
	WeaponSnipGroup.PlaceLabledControl("Maximum FOV", this, &WeaponSnipFoV);

	WeaponSnipRecoil.SetFileId("snip_recoil");
	WeaponSnipGroup.PlaceLabledControl("Ajust Recoil", this, &WeaponSnipRecoil);

	WeaponSnipPSilent.SetFileId("snip_psilent");
	WeaponSnipGroup.PlaceLabledControl("Silent Aim", this, &WeaponSnipPSilent);

	WeaponSnipInacc.SetFileId("snip_inacc");
	WeaponSnipInacc.SetBoundaries(0.f, 15.f);
	WeaponSnipGroup.PlaceLabledControl("Miss Chance", this, &WeaponSnipInacc);

	WeaponSnipHitbox.SetFileId("snip_hitbox");
	WeaponSnipHitbox.AddItem("Head");
	WeaponSnipHitbox.AddItem("Neck");
	WeaponSnipHitbox.AddItem("Chest");
	WeaponSnipHitbox.AddItem("Stomach");
	WeaponSnipHitbox.AddItem("Nearest");
	WeaponSnipGroup.PlaceLabledControl("Hitbox", this, &WeaponSnipHitbox);*/
#pragma endregion
}

void CVisualTab::Setup()
{
	SetTitle("c");

	ActiveLabel.SetPosition(32, 48);
	ActiveLabel.SetText("active");
	RegisterControl(&ActiveLabel);

	Active.SetFileId("active");
	Active.SetPosition(78, 48);
	RegisterControl(&Active);

#pragma region Options
	OptionsGroup.SetText("general");
	OptionsGroup.SetPosition(16, 42);
	OptionsGroup.SetSize(376, 460);
	RegisterControl(&OptionsGroup);

	FiltersEnemiesOnly.SetFileId("ftr_enemyonly");
	OptionsGroup.PlaceLabledControl("enemy only", this, &FiltersEnemiesOnly);

	OptionsBox.SetFileId("otr_recoilhair");
/*	OptionsBox.AddItem("Off");
	OptionsBox.AddItem("Full");
	OptionsBox.AddItem("Corner");*/
	OptionsGroup.PlaceLabledControl("draw box", this, &OptionsBox);

	OptionsFillBox.SetFileId("opt_Fill");
	OptionsGroup.PlaceLabledControl("box fill", this, &OptionsFillBox);

	OptionsName.SetFileId("opt_name");
	OptionsGroup.PlaceLabledControl("draw name", this, &OptionsName);

	OptionsVitals.SetFileId("opt_hp");
	OptionsVitals.AddItem("Off");
	OptionsVitals.AddItem("normal");
	OptionsVitals.AddItem("sector");
	OptionsGroup.PlaceLabledControl("draw hp", this, &OptionsVitals);

	OptionsWeapon.SetFileId("opt_weapon");
	OptionsGroup.PlaceLabledControl("weapon esp", this, &OptionsWeapon);

	OptionsInfo.SetFileId("opt_info");
	OptionsGroup.PlaceLabledControl("misc info", this, &OptionsInfo);

	BacktrackingLol.SetFileId("opt_backdot");
	OptionsGroup.PlaceLabledControl("backtrack draw", this, &BacktrackingLol);

	OptionsChams.SetFileId("opt_chams");
	OptionsChams.AddItem("off");
	OptionsChams.AddItem("material");
	OptionsChams.AddItem("flat");
	OptionsGroup.PlaceLabledControl("chams", this, &OptionsChams);

	ChamsXQZ.SetFileId("opt_xqz");
	OptionsGroup.PlaceLabledControl("chams thru walls", this, &ChamsXQZ);

	OptionsSkeleton.SetFileId("opt_bone");
	OptionsGroup.PlaceLabledControl("draw skeleton", this, &OptionsSkeleton);

	/*OptionsGlow.SetFileId("opt_glow");
	OptionsGroup.PlaceLabledControl("Glow", this, &OptionsGlow);*/

//	OptionsAimSpot.SetFileId("opt_aimspot");
//	OptionsGroup.PlaceLabledControl("Head Cross", this, &OptionsAimSpot);

//	OptionsCompRank.SetFileId("opt_comprank");
//	OptionsGroup.PlaceLabledControl("Player Ranks", this, &OptionsCompRank);


	FiltersWeapons.SetFileId("ftr_weaps");
	OptionsGroup.PlaceLabledControl("draw items", this, &FiltersWeapons);

	ShowImpacts.SetFileId("opt_impacts");
	OptionsGroup.PlaceLabledControl("draw impacts", this, &ShowImpacts);

//	GrenadeTracer.SetFileId("opt_grentrail");
//	OptionsGroup.PlaceLabledControl("Show Grenade Trail", this, &GrenadeTracer);

	IsScoped.SetFileId("opt_scoped");
	OptionsGroup.PlaceLabledControl("draw if scoped", this, &IsScoped);

	HasDefuser.SetFileId("opt_dontbealoserbuyadefuser");
	OptionsGroup.PlaceLabledControl("draw if has defuser", this, &HasDefuser);

	IsDefusing.SetFileId("opt_hedufusinglel");
	OptionsGroup.PlaceLabledControl("draw if defusing", this, &IsDefusing);

	RemoveScope.SetFileId("opt_RemoveScope");
	OptionsGroup.PlaceLabledControl("don't draw scope", this, &RemoveScope);

	OptionsBarrel.SetFileId("aa_Barrel");
	OptionsGroup.PlaceLabledControl("draw tracers", this, &OptionsBarrel);

/*	OptionsLBY.SetFileId("aa_LBY");
	OptionsGroup.PlaceLabledControl("LBY indicator", this, &OptionsLBY);*/

	//OtherGlow.SetFileId("aa_glow");
	//OptionsGroup.PlaceLabledControl("Spoof Spectator", this, &OtherGlow);

#pragma endregion Setting up the Options controls

/*#pragma region Filters
	FiltersGroup.SetText("Filters");
	FiltersGroup.SetPosition(225, 48);
	FiltersGroup.SetSize(193, 430);
	RegisterControl(&FiltersGroup);

	//FiltersAll.SetFileId("ftr_all");
	//FiltersGroup.PlaceLabledControl("All", this, &FiltersAll);

	//FiltersPlayers.SetFileId("ftr_players");
	//FiltersGroup.PlaceLabledControl("Players", this, &FiltersPlayers);

//	FiltersChickens.SetFileId("ftr_chickens");
//	FiltersGroup.PlaceLabledControl("Chickens", this, &FiltersChickens);

	//FiltersC4.SetFileId("ftr_c4");
	//FiltersGroup.PlaceLabledControl("Bomb", this, &FiltersC4);
#pragma endregion Setting up the Filters controls*/

#pragma region Other
	OtherGroup.SetText("misc");
	OtherGroup.SetPosition(408, 48);
	OtherGroup.SetSize(360, 360);
	RegisterControl(&OtherGroup);

	OtherSniperCrosshair.SetFileId("otr_xhair");
	OtherGroup.PlaceLabledControl("sniper crosshair", this, &OtherSniperCrosshair);

	OtherSpreadCrosshair.SetFileId("otr_spreadhair");
	OtherGroup.PlaceLabledControl("draw spread", this, &OtherSpreadCrosshair);

/*	OtherRecoilCrosshair.SetFileId("otr_recoilhair");
	OtherRecoilCrosshair.AddItem("Off");
	OtherRecoilCrosshair.AddItem("Recoil");
	OtherRecoilCrosshair.AddItem("Spread");
	OtherGroup.PlaceLabledControl("Dynamic Crosshair", this, &OtherRecoilCrosshair);*/

	//OtherRadar.SetFileId("otr_radar");
	//OtherGroup.PlaceLabledControl("Radar", this, &OtherRadar);

	OtherNoVisualRecoil.SetFileId("otr_visrecoil");
	OtherGroup.PlaceLabledControl("adjust vis recoil punch", this, &OtherNoVisualRecoil);

	OtherNoFlash.SetFileId("otr_noflash");
	OtherGroup.PlaceLabledControl("don't draw flash", this, &OtherNoFlash);

	OtherNoSmoke.SetFileId("otr_nosmoke");
	OtherGroup.PlaceLabledControl("don't draw smoke", this, &OtherNoSmoke);

	OtherPlayerLine.SetFileId("otr_angllins");
	OtherGroup.PlaceLabledControl("draw fake angles", this, &OtherPlayerLine);

	LbyIndic.SetFileId("otr_indicator");
	OtherGroup.PlaceLabledControl("draw indicator", this, &LbyIndic);


//	OtherEnemyCircle.SetFileId("otr_enemccirc");
//	OtherGroup.PlaceLabledControl("Enemy Circle", this, &OtherEnemyCircle);

	OtherNoHands.SetFileId("otr_hands");
	OtherNoHands.AddItem("off");
	OtherNoHands.AddItem("invisible");
	OtherNoHands.AddItem("glass");
	OtherNoHands.AddItem("goldie");
	OtherNoHands.AddItem("wireframe");
	OtherNoHands.AddItem("biedronka nie ma ogonka");
	OtherGroup.PlaceLabledControl("hand options", this, &OtherNoHands);

	HandXQZ.SetFileId("otr_handxqz");
	OtherGroup.PlaceLabledControl("hands thru wall", this, &HandXQZ);

	OtherFOV.SetFileId("otr_fov");
	OtherFOV.SetBoundaries(0.f, 180.f);
	OtherFOV.SetValue(90.f);
	OtherGroup.PlaceLabledControl("custom fov", this, &OtherFOV);

	OtherViewmodelFOV.SetFileId("otr_viewfov");
	OtherViewmodelFOV.SetBoundaries(0.f, 180.f);
	OtherViewmodelFOV.SetValue(0.f);
	OtherGroup.PlaceLabledControl("custom viewmodel fov", this, &OtherViewmodelFOV);

	/*Test.SetFileId("rgb_test");
	Test.SetBoundaries(0.f, 0.9f);
	Test.SetValue(0.f);
	OtherGroup.PlaceLabledControl(" ", this, &Test);*/

#pragma endregion Setting up the Other controls

#pragma region WorldOptions
/*	CvarGroup.SetText("World");
	CvarGroup.SetPosition(434, 320);
	CvarGroup.SetSize(334, 170);
	RegisterControl(&CvarGroup);

	NightMode.SetFileId("otr_nightmode");
	NightMode.SetState(false);
	CvarGroup.PlaceLabledControl("Night-Mode", this, &NightMode);

	AmbientExposure.SetFileId("otr_ambientexposure");
	AmbientExposure.SetBoundaries(0.f, 2.f);
	AmbientExposure.SetValue(2.f);
	CvarGroup.PlaceLabledControl("Ambient-Exposure", this, &AmbientExposure);

	AmbientRed.SetFileId("otr_ambientred");
	AmbientRed.SetBoundaries(0.f, .5f);
	AmbientRed.SetValue(0.f);
	CvarGroup.PlaceLabledControl("Ambient-Red", this, &AmbientRed);

	AmbientGreen.SetFileId("otr_ambientgreen");
	AmbientGreen.SetBoundaries(0.f, .5f);
	AmbientGreen.SetValue(0.f);
	CvarGroup.PlaceLabledControl("Ambient-Green", this, &AmbientGreen);

	AmbientBlue.SetFileId("otr_ambientblue");
	AmbientBlue.SetBoundaries(0.f, .5f);
	AmbientBlue.SetValue(0.f);
	CvarGroup.PlaceLabledControl("Ambient-Blue", this, &AmbientBlue);

	AmbientSkybox.SetFileId("otr_skyboxchanger");
	AmbientSkybox.AddItem("Disabled");
	AmbientSkybox.AddItem("Skybox 1");
	AmbientSkybox.AddItem("Skybox 2");
	AmbientSkybox.AddItem("Skybox 3");
	AmbientSkybox.AddItem("Skybox 4");
	CvarGroup.PlaceLabledControl("Change-Skybox", this, &AmbientSkybox);*/

#pragma endregion Setting up the Other controls
}

void CMiscTab::Setup()
{
	SetTitle("d");

/*#pragma region Knife
	KnifeGroup.SetPosition(16, 16);
	KnifeGroup.SetSize(360, 126);
	KnifeGroup.SetText("Knife Changer");
	RegisterControl(&KnifeGroup);

	KnifeEnable.SetFileId("knife_enable");
	KnifeGroup.PlaceLabledControl("Enable", this, &KnifeEnable);

	KnifeModel.SetFileId("knife_model");
	KnifeModel.AddItem("Karam");
	KnifeModel.AddItem("Bayo");
	KnifeGroup.PlaceLabledControl("Knife", this, &KnifeModel);

	KnifeSkin.SetFileId("knife_skin");
	KnifeSkin.AddItem("Doppler Sapphire");
	KnifeSkin.AddItem("Doppler Ruby");
	KnifeSkin.AddItem("Tiger");
	KnifeSkin.AddItem("Lore");
	KnifeGroup.PlaceLabledControl("Skin", this, &KnifeSkin);

	KnifeApply.SetText("Apply Knife");
	KnifeApply.SetCallback(KnifeApplyCallbk);
	KnifeGroup.PlaceLabledControl("", this, &KnifeApply);

#pragma endregion*/

#pragma region Other
	OtherGroup.SetPosition(16, 48);
	OtherGroup.SetSize(360, 430);
	OtherGroup.SetText("other");
	RegisterControl(&OtherGroup);

	OtherAutoJump.SetFileId("otr_autojump");
	OtherGroup.PlaceLabledControl("automatic jump", this, &OtherAutoJump);

	//	OtherEdgeJump.SetFileId("otr_edgejump");
	//	OtherGroup.PlaceLabledControl("Edge Jump", this, &OtherEdgeJump);

	OtherAutoStrafe.SetFileId("otr_strafe");
	OtherAutoStrafe.AddItem("off");
	OtherAutoStrafe.AddItem("legitly");
	OtherAutoStrafe.AddItem("rage");
	//	OtherAutoStrafe.AddItem("Telestrafe");
	OtherGroup.PlaceLabledControl("automatic strafer", this, &OtherAutoStrafe);

	OtherSafeMode.SetFileId("otr_safemode");
	OtherSafeMode.SetState(true);
	OtherGroup.PlaceLabledControl("spread mode", this, &OtherSafeMode);

	/*	OtherChatSpam.SetFileId("otr_spam");
	OtherChatSpam.AddItem("Off");
	OtherChatSpam.AddItem("Namestealer");
	OtherChatSpam.AddItem("plaSticware spam");
	OtherChatSpam.AddItem("CRASHER MEME");
	OtherChatSpam.AddItem("plaSticware");
	OtherGroup.PlaceLabledControl("NameChanger/spam", this, &OtherChatSpam);*/

	OtherClantag.SetFileId("otr_clantag");
//	OtherClantag.AddItem("Off");
	//OtherClantag.AddItem("skeet.cc");
//	OtherClantag.AddItem("Halloween");
//	OtherClantag.AddItem("HolidayHack");
	OtherGroup.PlaceLabledControl("clantag sync", this, &OtherClantag);

	//	OtherTeamChat.SetFileId("otr_teamchat");
	//	OtherGroup.PlaceLabledControl("Team Chat Only", this, &OtherTeamChat);

	//	OtherChatDelay.SetFileId("otr_chatdelay");
	//	OtherChatDelay.SetBoundaries(0.1, 3.0);
	//	OtherChatDelay.SetValue(0.5);
	//	OtherGroup.PlaceLabledControl("Spam Delay", this, &OtherChatDelay);
	//OtherCrasher.SetFileId("otr_crash");
	//OtherGroup.PlaceLabledControl("paste crasher", this, &OtherCrasher);

	OtherAirStuck.SetFileId("otr_astuck");
	OtherGroup.PlaceLabledControl("stuck in air", this, &OtherAirStuck);

//	OtherSpectators.SetFileId("otr_speclist");
//	OtherGroup.PlaceLabledControl("Spectators List", this, &OtherSpectators);

//	OtherThirdperson.SetFileId("aa_thirdpsr");
//	OtherGroup.PlaceLabledControl("Thirdperson", this, &OtherThirdperson);

	legend.SetFileId("otr_legendaryman");
	OtherGroup.PlaceLabledControl("wb name", this, &legend);

	memespam.SetFileId("realistic_mode");
	OtherGroup.PlaceLabledControl("meme spam", this, &memespam);

	OtherHitmarker.SetFileId("otr_hitmarker");
	OtherGroup.PlaceLabledControl("hitmarker", this, &OtherHitmarker);

	OtherHitmarkerType.SetFileId("otr_hitsound");
	OtherHitmarkerType.AddItem("normal");
	OtherHitmarkerType.AddItem("skeet");
	OtherHitmarkerType.AddItem("test");
	OtherGroup.PlaceLabledControl("hitmarker type", this, &OtherHitmarkerType);

	OtherThirdpersonKey.SetFileId("otr_tpkeylul");
	OtherGroup.PlaceLabledControl("thirdperson key", this, &OtherThirdpersonKey);

	OtherThirdpersonType.SetFileId("otr_tptypelul");
	OtherThirdpersonType.AddItem("real angle");
	OtherThirdpersonType.AddItem("fake angle");
	OtherGroup.PlaceLabledControl("thirdperson type", this, &OtherThirdpersonType);

	FakeWalk.SetFileId("otr_FakeWalk");
	OtherGroup.PlaceLabledControl("faked walking", this, &FakeWalk);
#pragma endregion other random options

#pragma region FakeLag
	FakeLagGroup.SetPosition(408, 48);
	FakeLagGroup.SetSize(360, 180);
	FakeLagGroup.SetText("fun");
	RegisterControl(&FakeLagGroup);

	FakeLagEnable.SetFileId("fakelag_enable");
	FakeLagGroup.PlaceLabledControl("enabled", this, &FakeLagEnable);

	FakeLagChoke.SetFileId("fakelag_choke");
	FakeLagChoke.SetBoundaries(0, 18);
	FakeLagChoke.SetValue(0);
	FakeLagGroup.PlaceLabledControl("packet modulator choke", this, &FakeLagChoke);

	FakeLagTyp.SetFileId("fakelag_typ");
	FakeLagTyp.AddItem("Off");
	FakeLagTyp.AddItem("static");
	FakeLagTyp.AddItem("switching");
	FakeLagTyp.AddItem("adaptive");
	FakeLagGroup.PlaceLabledControl("fakelag type", this, &FakeLagTyp);

	OtherMemewalk.SetFileId("otr_slidewalk");
	FakeLagGroup.PlaceLabledControl("memewalk", this, &OtherMemewalk);


#pragma endregion fakelag shi

//	ChokeRandomize.SetFileId("choke_random");
//	FakeLagGroup.PlaceLabledControl("Randomize Choke", this, &ChokeRandomize);

//	SendRandomize.SetFileId("send_randome");
//	FakeLagGroup.PlaceLabledControl("Randomize Send", this, &SendRandomize);

//	SendRandomize.SetFileId("fakelag_InAir");
//	FakeLagGroup.PlaceLabledControl("Fakelag in Air", this, &InAir);*/


#pragma endregion fakelag shit


#pragma endregion


#pragma endregion  AntiAim controls get setup in here


	/*#pragma region Teleport
	TeleportGroup.SetPosition(16, 346);
	TeleportGroup.SetSize(360, 75);
	TeleportGroup.SetText("Teleport");
	RegisterControl(&TeleportGroup);

	TeleportEnable.SetFileId("teleport_enable");
	TeleportGroup.PlaceLabledControl("Enable", this, &TeleportEnable);

	TeleportKey.SetFileId("teleport_key");
	TeleportGroup.PlaceLabledControl("Key", this, &TeleportKey);*/

#pragma endregion

	/*#pragma region OverideFov
	FOVGroup.SetPosition(16, 365);
	FOVGroup.SetSize(360, 75);
	FOVGroup.SetText("FOV Changer");
	RegisterControl(&FOVGroup);

	FOVEnable.SetFileId("fov_enable");
	FOVGroup.PlaceLabledControl("Enable", this, &FOVEnable);

	FOVSlider.SetFileId("fov_slider");
	FOVSlider.SetBoundaries(0, 200);
	FOVSlider.SetValue(0);
	FOVGroup.PlaceLabledControl("FOV Amount", this, &FOVSlider);

	#pragma endregion*/
}
//
void CAntiAimTab::Setup()
{
	SetTitle("f"); //I //J

#pragma region AntiAim
	AntiAimGroup.SetPosition(16, 48); //344
	AntiAimGroup.SetText("anti-aims");
	AntiAimGroup.SetSize(376, 315);
	RegisterControl(&AntiAimGroup);

	AntiAimEnable.SetFileId("aa_enable");
	AntiAimGroup.PlaceLabledControl("enable", this, &AntiAimEnable);

	AntiAimPitch.SetFileId("aa_x");
	AntiAimPitch.AddItem("off");
	AntiAimPitch.AddItem("emotion");
	AntiAimPitch.AddItem("lby pitch (XD)");
	AntiAimPitch.AddItem("fake zero");
//	AntiAimPitch.AddItem("Fake Down");
//	AntiAimPitch.AddItem("Fake Up");
//	AntiAimPitch.AddItem("Fake Zero");
//	AntiAimPitch.AddItem("Fake - 90");
//	AntiAimPitch.AddItem("Jitter");
//	AntiAimPitch.AddItem("Mixed");
	AntiAimGroup.PlaceLabledControl("real pitch", this, &AntiAimPitch);

	staticyaw.SetFileId("aa_y");
	staticyaw.AddItem("off");//0
	staticyaw.AddItem("180");//1
	staticyaw.AddItem("sideway");
	staticyaw.AddItem("automatic");//2
	staticyaw.AddItem("sideways jitter");//3
	staticyaw.AddItem("180 jitter");//4
	staticyaw.AddItem("lby");//5
	staticyaw.AddItem("random");//6
	staticyaw.AddItem("spin");//7
	staticyaw.AddItem("fake static");
	staticyaw.AddItem("crash resolver");
	staticyaw.AddItem("lby plus");
	staticyaw.AddItem("fake -90");
	AntiAimGroup.PlaceLabledControl("real stand", this, &staticyaw);

//	SpinSpeed.SetFileId("aa_staticreal");
//	SpinSpeed.SetBoundaries(0.f, 180.f);
//	SpinSpeed.SetValue(0.f);
//	AntiAimGroup.PlaceLabledControl("Static Modifier", this, &SpinSpeed);

	fakeyaw.SetFileId("aa_idley");//0
	fakeyaw.AddItem("off");
	fakeyaw.AddItem("180");//1
	fakeyaw.AddItem("sideway");//2
	fakeyaw.AddItem("automatic");//2
	fakeyaw.AddItem("sideways jitter");//3
	fakeyaw.AddItem("180 jitter");//4
	fakeyaw.AddItem("lby");//5
	fakeyaw.AddItem("random");//6
	fakeyaw.AddItem("spin");//7
	fakeyaw.AddItem("fake static");
	fakeyaw.AddItem("crash resolver");
	fakeyaw.AddItem("lby plus");
	fakeyaw.AddItem("fake -90");
	AntiAimGroup.PlaceLabledControl("fake stand", this, &fakeyaw);

	moveyaw.SetFileId("aa_movey");
	moveyaw.AddItem("off");
	moveyaw.AddItem("180");//1
	moveyaw.AddItem("sideway");//2
	moveyaw.AddItem("automatic");//2
	moveyaw.AddItem("sideways jitter");//3
	moveyaw.AddItem("180 jitter");//4
	moveyaw.AddItem("lby");//5
	moveyaw.AddItem("random");//6
	moveyaw.AddItem("spin");//7
	moveyaw.AddItem("fake static");
	moveyaw.AddItem("crash resolver");
	moveyaw.AddItem("lby plus");
	moveyaw.AddItem("fake -90");
	AntiAimGroup.PlaceLabledControl("real move", this, &moveyaw);

	fakemove.SetFileId("aa_movefakey");
	fakemove.AddItem("off");
	fakemove.AddItem("180");//1
	fakemove.AddItem("sideway");//2
	fakemove.AddItem("automatic");//2
	fakemove.AddItem("sideways jitter");//3
	fakemove.AddItem("180 jitter");//4
	fakemove.AddItem("lby");//5
	fakemove.AddItem("random");//6
	fakemove.AddItem("spin");//7
	fakemove.AddItem("fake static");
	fakemove.AddItem("crash resolver");
	fakemove.AddItem("lby plus");
	fakemove.AddItem("fake -90");
	AntiAimGroup.PlaceLabledControl("fake move", this, &fakemove);

//	SpinSpeed2.SetFileId("aa_staticfake");
//	SpinSpeed2.SetBoundaries(0.f, 180.f);
//	SpinSpeed2.SetValue(0.f);
//	AntiAimGroup.PlaceLabledControl("Static Modifier", this, &SpinSpeed2);
/*	movingyaw.SetFileId("aa_ymving");
	movingyaw.AddItem("None");//0
	movingyaw.AddItem("Backwards");//1
	movingyaw.AddItem("Sideways");//2
	movingyaw.AddItem("Sideways Jitter");//3
	movingyaw.AddItem("Backwards Jitter");//4
	movingyaw.AddItem("Lowerbody");//5
	movingyaw.AddItem("Spinbot");//6
	AntiAimGroup.PlaceLabledControl("Yaw (Moving Override)", this, &movingyaw);*/

/*	edgeyaw.SetFileId("aa_yedge");
	edgeyaw.AddItem("None");//0
	edgeyaw.AddItem("Head Hide");//1
	edgeyaw.AddItem("Fake Out");//2
	edgeyaw.AddItem("Fake Animation");//3
	edgeyaw.AddItem("Lowerbody");//4
	AntiAimGroup.PlaceLabledControl("Yaw (Edge Override)", this, &edgeyaw);*/

	FlipKey.SetFileId("aim_key");
	AntiAimGroup.PlaceLabledControl("flip on key", this, &FlipKey);

	AntiAimOffset.SetFileId("aa_offset");
	AntiAimOffset.SetBoundaries(0.f, 360.f);
	AntiAimOffset.SetValue(0.f);
	AntiAimGroup.PlaceLabledControl("jitter mod", this, &AntiAimOffset);

	SpinSpeed.SetFileId("aa_AntiAimSpinspeed");
	SpinSpeed.SetBoundaries(0.f, 100.f);
	SpinSpeed.SetValue(0.f);
	AntiAimGroup.PlaceLabledControl("spinning speed", this, &SpinSpeed);

	NoSpreadServer.SetFileId("aa_nonspread");
	AntiAimGroup.PlaceLabledControl("non-spread hvh", this, &NoSpreadServer);

	BreakLBY.SetFileId("aa_breaklby");
	AntiAimGroup.PlaceLabledControl("anti-lby correct", this, &BreakLBY);


//	AntiAimTarget.SetFileId("aa_target");
//	AntiAimGroup.PlaceLabledControl("At Target", this, &AntiAimTarget);

//	AntiAimJitterLBY.SetFileId("aa_AntiAimJitterLBY");
//	AntiAimGroup.PlaceLabledControl("Anti Resolver", this, &AntiAimJitterLBY);



/*	AntiResolver.SetFileId("aa_AntiResolver");
	AntiResolver.AddItem("Off");
	AntiResolver.AddItem("Force");
	AntiResolver.AddItem("LBY");
	AntiResolver.AddItem("Auto Flip");

	AntiAimGroup.PlaceLabledControl("AntiResolver", this, &AntiResolver);*/

	MiscAA.SetPosition(408, 48);
	MiscAA.SetSize(360, 180);
	MiscAA.SetText("misc");
	RegisterControl(&MiscAA);

	AntiAimKnife.SetFileId("aa_withknife");
	MiscAA.PlaceLabledControl("disable when with knife", this, &AntiAimKnife);

	AntiResolver.SetFileId("aa_antiresolver");
	AntiResolver.AddItem("off");
	AntiResolver.AddItem("basic");
	AntiResolver.AddItem("paste");
	AntiResolver.AddItem("anti-aimware");
	MiscAA.PlaceLabledControl("anti-correct", this, &AntiResolver);

//	NoPlayers.SetFileId("aa_noenemy");
//	MiscAA.PlaceLabledControl("no enemys", this, &NoPlayers);

}

void CColorTab::Setup()
{
	SetTitle("COLORS");

#pragma region Player ESP

	ColorGroup.SetPosition(16, 16); //344
	ColorGroup.SetText("Player ESP");
	ColorGroup.SetSize(376, 560);
	RegisterControl(&ColorGroup);

	FillBoxRed.SetFileId("color_fillbox");
	ColorGroup.PlaceLabledControl("FillBox", this, &PlayerEsp);
	FillBoxRed.SetBoundaries(0.f, 255.f);
	FillBoxRed.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Red", this, &FillBoxRed);
	FillBoxGreen.SetBoundaries(0.f, 255.f);
	FillBoxGreen.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Green", this, &FillBoxGreen);
	FillBoxBlue.SetBoundaries(0.f, 255.f);
	FillBoxBlue.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Blue", this, &FillBoxBlue);
	FillBoxAlpha.SetBoundaries(0.f, 255.f);
	FillBoxAlpha.SetValue(50.f);
	ColorGroup.PlaceLabledControl("Alpha", this, &FillBoxAlpha);

	FillBoxRed.SetFileId("color_fillbox");
	ColorGroup.PlaceLabledControl("Terrorist Chams", this, &PlayerEsp);
	CTChamsRed.SetBoundaries(0.f, 255.f);
	CTChamsRed.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Red", this, &CTChamsRed);
	CTChamsGreen.SetBoundaries(0.f, 255.f);
	CTChamsGreen.SetValue(255.f);
	ColorGroup.PlaceLabledControl("Green", this, &CTChamsGreen);
	CTChamsBlue.SetBoundaries(0.f, 255.f);
	CTChamsBlue.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Blue", this, &CTChamsBlue);

	FillBoxRed.SetFileId("color_fillbox");
	ColorGroup.PlaceLabledControl("Terrorist Chams Vis", this, &PlayerEsp);
	CTVisChamsRed.SetBoundaries(0.f, 255.f);
	CTVisChamsRed.SetValue(255.f);
	ColorGroup.PlaceLabledControl("Red", this, &CTVisChamsRed);
	CTVisChamsGreen.SetBoundaries(0.f, 255.f);
	CTVisChamsGreen.SetValue(255.f);
	ColorGroup.PlaceLabledControl("Green", this, &CTVisChamsGreen);
	CTVisChamsBlue.SetBoundaries(0.f, 255.f);
	CTVisChamsBlue.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Blue", this, &CTVisChamsBlue);

	FillBoxRed.SetFileId("color_fillbox");
	ColorGroup.PlaceLabledControl("CT Vis Chams", this, &PlayerEsp);
	TerroristVisChamsRed.SetBoundaries(0.f, 255.f);
	TerroristVisChamsRed.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Red", this, &TerroristVisChamsRed);
	TerroristVisChamsGreen.SetBoundaries(0.f, 255.f);
	TerroristVisChamsGreen.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Green", this, &TerroristVisChamsGreen);
	TerroristVisChamsBlue.SetBoundaries(0.f, 255.f);
	TerroristVisChamsBlue.SetValue(255.f);
	ColorGroup.PlaceLabledControl("Blue", this, &TerroristVisChamsBlue);

	FillBoxRed.SetFileId("color_fillbox");
	ColorGroup.PlaceLabledControl("CT Chams", this, &PlayerEsp);
	TerroristChamsRed.SetBoundaries(0.f, 255.f);
	TerroristChamsRed.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Red", this, &TerroristChamsRed);
	TerroristChamsGreen.SetBoundaries(0.f, 255.f);
	TerroristChamsGreen.SetValue(255.f);
	ColorGroup.PlaceLabledControl("Green", this, &TerroristChamsGreen);
	TerroristChamsBlue.SetBoundaries(0.f, 255.f);
	TerroristChamsBlue.SetValue(0.f);
	ColorGroup.PlaceLabledControl("Blue", this, &TerroristChamsBlue);


#pragma endregion Player ESP


#pragma region Terrorist ESP

	TerroristColorGroup.SetPosition(408, 306);
	TerroristColorGroup.SetSize(360, 270);
	TerroristColorGroup.SetText("Terrorist ESP");
	RegisterControl(&TerroristColorGroup);

	VisBoxRed.SetFileId("color_box");
	TerroristColorGroup.PlaceLabledControl("Terrorist Box visable", this, &PlayerEsp);
	VisBoxRed.SetBoundaries(0.f, 255.f);
	VisBoxRed.SetValue(0.f);
	TerroristColorGroup.PlaceLabledControl("Red", this, &VisBoxRed);
	VisBoxGreen.SetBoundaries(0.f, 255.f);
	VisBoxGreen.SetValue(255.f);
	TerroristColorGroup.PlaceLabledControl("Green", this, &VisBoxGreen);
	VisBoxBlue.SetBoundaries(0.f, 255.f);
	VisBoxBlue.SetValue(0.f);
	TerroristColorGroup.PlaceLabledControl("Blue", this, &VisBoxBlue);
	VisBoxAlpha.SetBoundaries(0.f, 255.f);
	VisBoxAlpha.SetValue(255.f);
	TerroristColorGroup.PlaceLabledControl("Alpha", this, &VisBoxAlpha);

	BoxRed.SetFileId("color_box");
	TerroristColorGroup.PlaceLabledControl("Terrorist Box", this, &PlayerEsp);
	BoxRed.SetBoundaries(0.f, 255.f);
	BoxRed.SetValue(0.f);
	TerroristColorGroup.PlaceLabledControl("Red", this, &BoxRed);
	BoxGreen.SetBoundaries(0.f, 255.f);
	BoxGreen.SetValue(0.f);
	TerroristColorGroup.PlaceLabledControl("Green", this, &BoxGreen);
	BoxBlue.SetBoundaries(0.f, 255.f);
	BoxBlue.SetValue(255.f);
	TerroristColorGroup.PlaceLabledControl("Blue", this, &BoxBlue);
	BoxAlpha.SetBoundaries(0.f, 255.f);
	BoxAlpha.SetValue(255.f);
	TerroristColorGroup.PlaceLabledControl("Alpha", this, &BoxAlpha);

#pragma endregion Terrorist ESP


#pragma region CT ESP

	CTColorGroup.SetPosition(408, 16);
	CTColorGroup.SetSize(360, 270);
	CTColorGroup.SetText("CT ESP");
	RegisterControl(&CTColorGroup);

	VisBoxRed2.SetFileId("color_box");
	CTColorGroup.PlaceLabledControl("CT Box visable", this, &PlayerEsp);
	VisBoxRed2.SetBoundaries(0.f, 255.f);
	VisBoxRed2.SetValue(0.f);
	CTColorGroup.PlaceLabledControl("Red", this, &VisBoxRed2);
	VisBoxGreen2.SetBoundaries(0.f, 255.f);
	VisBoxGreen2.SetValue(255.f);
	CTColorGroup.PlaceLabledControl("Green", this, &VisBoxGreen2);
	VisBoxBlue2.SetBoundaries(0.f, 255.f);
	VisBoxBlue2.SetValue(0.f);
	CTColorGroup.PlaceLabledControl("Blue", this, &VisBoxBlue2);
	VisBoxAlpha2.SetBoundaries(0.f, 255.f);
	VisBoxAlpha2.SetValue(255.f);
	CTColorGroup.PlaceLabledControl("Alpha", this, &VisBoxAlpha2);

	BoxRed2.SetFileId("color_box");
	CTColorGroup.PlaceLabledControl("CT Box ", this, &PlayerEsp);
	BoxRed2.SetBoundaries(0.f, 255.f);
	BoxRed2.SetValue(0.f);
	CTColorGroup.PlaceLabledControl("Red", this, &BoxRed2);
	BoxGreen2.SetBoundaries(0.f, 255.f);
	BoxGreen2.SetValue(0.f);
	CTColorGroup.PlaceLabledControl("Green", this, &BoxGreen2);
	BoxBlue2.SetBoundaries(0.f, 255.f);
	BoxBlue2.SetValue(255.f);
	CTColorGroup.PlaceLabledControl("Blue", this, &BoxBlue2);
	BoxAlpha2.SetBoundaries(0.f, 255.f);
	BoxAlpha2.SetValue(255.f);
	CTColorGroup.PlaceLabledControl("Alpha", this, &BoxAlpha2);

#pragma endregion CT ESP
}

void SkinTab::Setup()
{
	SetTitle("SKINS");

	SkinActive.SetPosition(16, 16);
	SkinActive.SetText("Active");
	RegisterControl(&SkinActive);

	SkinEnable.SetFileId("skin_enable");
	SkinEnable.SetPosition(66, 16);
	RegisterControl(&SkinEnable);

	SkinApply.SetText("Apply");
	SkinApply.SetCallback(KnifeApplyCallbk);
	SkinApply.SetPosition(408, 490);
	SkinApply.SetSize(360, 106);
	RegisterControl(&SkinApply);

#pragma region Knife
	KnifeGroup.SetPosition(16, 48);
	KnifeGroup.SetText("Knife");
	KnifeGroup.SetSize(376, 80);
	RegisterControl(&KnifeGroup);

	KnifeModel.SetFileId("knife_model");
	KnifeModel.AddItem("Bayonet");
	KnifeModel.AddItem("Bowie Knife");
	KnifeModel.AddItem("Butterfly Knife");
	KnifeModel.AddItem("Falchion Knife");
	KnifeModel.AddItem("Flip Knife");
	KnifeModel.AddItem("Gut Knife");
	KnifeModel.AddItem("Huntsman Knife");
	KnifeModel.AddItem("Karambit");
	KnifeModel.AddItem("M9 Bayonet");
	KnifeModel.AddItem("Shadow Daggers");
	KnifeGroup.PlaceLabledControl("Knife", this, &KnifeModel);

	KnifeSkin.SetFileId("knife_skin");
	KnifeSkin.AddItem("None");
	KnifeSkin.AddItem("Crimson Web");
	KnifeSkin.AddItem("Bone Mask");
	KnifeSkin.AddItem("Fade");
	KnifeSkin.AddItem("Night");
	KnifeSkin.AddItem("Blue Steel");
	KnifeSkin.AddItem("Stained");
	KnifeSkin.AddItem("Case Hardened");
	KnifeSkin.AddItem("Slaughter");
	KnifeSkin.AddItem("Safari Mesh");
	KnifeSkin.AddItem("Boreal Forest");
	KnifeSkin.AddItem("Ultraviolet");
	KnifeSkin.AddItem("Urban Masked");
	KnifeSkin.AddItem("Scorched");
	KnifeSkin.AddItem("Rust Coat");
	KnifeSkin.AddItem("Tiger Tooth");
	KnifeSkin.AddItem("Damascus Steel");
	KnifeSkin.AddItem("Damascus Steel");
	KnifeSkin.AddItem("Marble Fade");
	KnifeSkin.AddItem("Rust Coat");
	KnifeSkin.AddItem("Doppler Ruby");
	KnifeSkin.AddItem("Doppler Sapphire");
	KnifeSkin.AddItem("Doppler Blackpearl");
	KnifeSkin.AddItem("Doppler Phase 1");
	KnifeSkin.AddItem("Doppler Phase 2");
	KnifeSkin.AddItem("Doppler Phase 3");
	KnifeSkin.AddItem("Doppler Phase 4");
	KnifeSkin.AddItem("Gamma Doppler Phase 1");
	KnifeSkin.AddItem("Gamma Doppler Phase 2");
	KnifeSkin.AddItem("Gamma Doppler Phase 3");
	KnifeSkin.AddItem("Gamma Doppler Phase 4");
	KnifeSkin.AddItem("Gamma Doppler Emerald");
	KnifeSkin.AddItem("Lore");
	KnifeGroup.PlaceLabledControl("Skin", this, &KnifeSkin);
#pragma endregion

#pragma region Machineguns
	MachinegunsGroup.SetPosition(408, 48);
	MachinegunsGroup.SetText("Machineguns");
	MachinegunsGroup.SetSize(360, 80);
	RegisterControl(&MachinegunsGroup);

	NEGEVSkin.SetFileId("negev_skin");
	NEGEVSkin.AddItem("Anodized Navy");
	NEGEVSkin.AddItem("Man-o'-war");
	NEGEVSkin.AddItem("Palm");
	NEGEVSkin.AddItem("VariCamo");
	NEGEVSkin.AddItem("Palm");
	NEGEVSkin.AddItem("CaliCamo");
	NEGEVSkin.AddItem("Terrain");
	NEGEVSkin.AddItem("Army Sheen");
	NEGEVSkin.AddItem("Bratatat");
	NEGEVSkin.AddItem("Desert-Strike");
	NEGEVSkin.AddItem("Nuclear Waste");
	NEGEVSkin.AddItem("Loudmouth");
	NEGEVSkin.AddItem("Power Loader");
	MachinegunsGroup.PlaceLabledControl("Negev", this, &NEGEVSkin);

	M249Skin.SetFileId("m249_skin");
	M249Skin.AddItem("Contrast Spray");
	M249Skin.AddItem("Blizzard Marbleized");
	M249Skin.AddItem("Jungle DDPAT");
	M249Skin.AddItem("Gator Mesh");
	M249Skin.AddItem("Magma");
	M249Skin.AddItem("System Lock");
	M249Skin.AddItem("Shipping Forecast");
	M249Skin.AddItem("Impact Drill");
	M249Skin.AddItem("Nebula Crusader");
	M249Skin.AddItem("Spectre");
	MachinegunsGroup.PlaceLabledControl("M249", this, &M249Skin);

#pragma endregion

#pragma region Snipers
	Snipergroup.SetPosition(16, 135);
	Snipergroup.SetText("Snipers");
	Snipergroup.SetSize(376, 125);
	RegisterControl(&Snipergroup);

	AWPSkin.SetFileId("awp_skin");
	AWPSkin.AddItem("BOOM");
	AWPSkin.AddItem("Dragon Lore");
	AWPSkin.AddItem("Pink DDPAT");
	AWPSkin.AddItem("Snake Camo");
	AWPSkin.AddItem("Lightning Strike");
	AWPSkin.AddItem("Safari Mesh");
	AWPSkin.AddItem("Corticera");
	AWPSkin.AddItem("Redline");
	AWPSkin.AddItem("Man-o'-war");
	AWPSkin.AddItem("Graphite");
	AWPSkin.AddItem("Electric Hive");
	AWPSkin.AddItem("Pit Viper");
	AWPSkin.AddItem("Asiimov");
	AWPSkin.AddItem("Worm God");
	AWPSkin.AddItem("Medusa");
	AWPSkin.AddItem("Sun in Leo");
	AWPSkin.AddItem("Hyper Beast");
	AWPSkin.AddItem("Elite Build");
	Snipergroup.PlaceLabledControl("AWP", this, &AWPSkin);

	SSG08Skin.SetFileId("sgg08_skin");
	SSG08Skin.AddItem("Lichen Dashed");
	SSG08Skin.AddItem("Dark Water");
	SSG08Skin.AddItem("Blue Spruce");
	SSG08Skin.AddItem("Sand Dune");
	SSG08Skin.AddItem("Palm");
	SSG08Skin.AddItem("Mayan Dreams");
	SSG08Skin.AddItem("Blood in the Water");
	SSG08Skin.AddItem("Tropical Storm");
	SSG08Skin.AddItem("Acid Fade");
	SSG08Skin.AddItem("Slashed");
	SSG08Skin.AddItem("Detour");
	SSG08Skin.AddItem("Abyss");
	SSG08Skin.AddItem("Big Iron");
	SSG08Skin.AddItem("Necropos");
	SSG08Skin.AddItem("Ghost Crusader");
	SSG08Skin.AddItem("Dragonfire");
	Snipergroup.PlaceLabledControl("SGG 08", this, &SSG08Skin);

	SCAR20Skin.SetFileId("scar20_skin");
	SCAR20Skin.AddItem("Splash Jam");
	SCAR20Skin.AddItem("Storm");
	SCAR20Skin.AddItem("Contractor");
	SCAR20Skin.AddItem("Carbon Fiber");
	SCAR20Skin.AddItem("Sand Mesh");
	SCAR20Skin.AddItem("Palm");
	SCAR20Skin.AddItem("Emerald");
	SCAR20Skin.AddItem("Crimson Web");
	SCAR20Skin.AddItem("Cardiac");
	SCAR20Skin.AddItem("Army Sheen");
	SCAR20Skin.AddItem("Cyrex");
	SCAR20Skin.AddItem("Grotto");
	SCAR20Skin.AddItem("Emerald");
	SCAR20Skin.AddItem("Green Marine");
	SCAR20Skin.AddItem("Outbreak");
	SCAR20Skin.AddItem("Bloodsport");
	Snipergroup.PlaceLabledControl("SCAR-20", this, &SCAR20Skin);

	G3SG1Skin.SetFileId("g3sg1_skin");
	G3SG1Skin.AddItem("Desert Storm");
	G3SG1Skin.AddItem("Arctic Camo");
	G3SG1Skin.AddItem("Bone Mask");
	G3SG1Skin.AddItem("Contractor");
	G3SG1Skin.AddItem("Safari Mesh");
	G3SG1Skin.AddItem("Polar Camo");
	G3SG1Skin.AddItem("Jungle Dashed");
	G3SG1Skin.AddItem("VariCamo");
	G3SG1Skin.AddItem("Predator");
	G3SG1Skin.AddItem("Demeter");
	G3SG1Skin.AddItem("Azure Zebra");
	G3SG1Skin.AddItem("Green Apple");
	G3SG1Skin.AddItem("Orange Kimono");
	G3SG1Skin.AddItem("Neon Kimono");
	G3SG1Skin.AddItem("Murky");
	G3SG1Skin.AddItem("Chronos");
	G3SG1Skin.AddItem("Flux");
	G3SG1Skin.AddItem("The Executioner");
	G3SG1Skin.AddItem("Orange Crash");
	Snipergroup.PlaceLabledControl("G3SG1", this, &G3SG1Skin);
#pragma endregion

#pragma region Shotguns
	Shotgungroup.SetPosition(408, 135);
	Shotgungroup.SetText("Shotguns");
	Shotgungroup.SetSize(360, 125);
	RegisterControl(&Shotgungroup);

	MAG7Skin.SetFileId("mag7_skin");
	MAG7Skin.AddItem("Counter Terrace");
	MAG7Skin.AddItem("Metallic DDPAT");
	MAG7Skin.AddItem("Silver");
	MAG7Skin.AddItem("Storm");
	MAG7Skin.AddItem("Bulldozer");
	MAG7Skin.AddItem("Heat");
	MAG7Skin.AddItem("Sand Dune");
	MAG7Skin.AddItem("Irradiated Alert");
	MAG7Skin.AddItem("Memento");
	MAG7Skin.AddItem("Hazard");
	MAG7Skin.AddItem("Heaven Guard");
	MAG7Skin.AddItem("Firestarter");
	MAG7Skin.AddItem("Seabird");
	MAG7Skin.AddItem("Cobalt Core");
	MAG7Skin.AddItem("Praetorian");
	Shotgungroup.PlaceLabledControl("Mag-7", this, &MAG7Skin);

	XM1014Skin.SetFileId("xm1014_skin");
	XM1014Skin.AddItem("Blaze Orange");
	XM1014Skin.AddItem("VariCamo Blue");
	XM1014Skin.AddItem("Bone Mask");
	XM1014Skin.AddItem("Blue Steel");
	XM1014Skin.AddItem("Blue Spruce");
	XM1014Skin.AddItem("Grassland");
	XM1014Skin.AddItem("Urban Perforated");
	XM1014Skin.AddItem("Jungle");
	XM1014Skin.AddItem("VariCamo");
	XM1014Skin.AddItem("VariCamo");
	XM1014Skin.AddItem("Fallout Warning");
	XM1014Skin.AddItem("Jungle");
	XM1014Skin.AddItem("CaliCamo");
	XM1014Skin.AddItem("Pit Viper");
	XM1014Skin.AddItem("Tranquility");
	XM1014Skin.AddItem("Red Python");
	XM1014Skin.AddItem("Heaven Guard");
	XM1014Skin.AddItem("Red Leather");
	XM1014Skin.AddItem("Bone Machine");
	XM1014Skin.AddItem("Quicksilver");
	XM1014Skin.AddItem("Scumbria");
	XM1014Skin.AddItem("Teclu Burner");
	XM1014Skin.AddItem("Black Tie");
	Shotgungroup.PlaceLabledControl("XM1014", this, &XM1014Skin);

	SAWEDOFFSkin.SetFileId("sawedoff_skin");
	SAWEDOFFSkin.AddItem("First Class");
	SAWEDOFFSkin.AddItem("Forest DDPAT");
	SAWEDOFFSkin.AddItem("Contrast Spray");
	SAWEDOFFSkin.AddItem("Snake Camo");
	SAWEDOFFSkin.AddItem("Orange DDPAT");
	SAWEDOFFSkin.AddItem("Fade");
	SAWEDOFFSkin.AddItem("Copper");
	SAWEDOFFSkin.AddItem("Origami");
	SAWEDOFFSkin.AddItem("Sage Spray");
	SAWEDOFFSkin.AddItem("VariCamo");
	SAWEDOFFSkin.AddItem("Irradiated Alert");
	SAWEDOFFSkin.AddItem("Mosaico");
	SAWEDOFFSkin.AddItem("Serenity");
	SAWEDOFFSkin.AddItem("Amber Fade");
	SAWEDOFFSkin.AddItem("Full Stop");
	SAWEDOFFSkin.AddItem("Highwayman");
	SAWEDOFFSkin.AddItem("The Kraken");
	SAWEDOFFSkin.AddItem("Rust Coat");
	SAWEDOFFSkin.AddItem("Bamboo Shadow");
	SAWEDOFFSkin.AddItem("Bamboo Forest");
	SAWEDOFFSkin.AddItem("Yorick");
	SAWEDOFFSkin.AddItem("Fubar");
	SAWEDOFFSkin.AddItem("Wasteland Princess");
	Shotgungroup.PlaceLabledControl("Sawed-Off", this, &SAWEDOFFSkin);

	NOVASkin.SetFileId("nova_skin");
	NOVASkin.AddItem("Candy Apple");
	NOVASkin.AddItem("Blaze Orange");
	NOVASkin.AddItem("Modern Hunter");
	NOVASkin.AddItem("Forest Leaves");
	NOVASkin.AddItem("Bloomstick");
	NOVASkin.AddItem("Sand Dune");
	NOVASkin.AddItem("Polar Mesh");
	NOVASkin.AddItem("Walnut");
	NOVASkin.AddItem("Predator");
	NOVASkin.AddItem("Tempest");
	NOVASkin.AddItem("Graphite");
	NOVASkin.AddItem("Ghost Camo");
	NOVASkin.AddItem("Rising Skull");
	NOVASkin.AddItem("Antique");
	NOVASkin.AddItem("Green Apple");
	NOVASkin.AddItem("Caged Steel");
	NOVASkin.AddItem("Koi");
	NOVASkin.AddItem("Moon in Libra");
	NOVASkin.AddItem("Ranger");
	NOVASkin.AddItem("HyperBeast");
	Shotgungroup.PlaceLabledControl("Nova", this, &NOVASkin);
#pragma endregion

#pragma region Rifles
	Riflegroup.SetPosition(16, 270);
	Riflegroup.SetText("Rifles");
	Riflegroup.SetSize(376, 195);
	RegisterControl(&Riflegroup);

	AK47Skin.SetFileId("ak47_skin");
	AK47Skin.AddItem("First Class");
	AK47Skin.AddItem("Red Laminate");
	AK47Skin.AddItem("Case Hardened");
	AK47Skin.AddItem("Black Laminate");
	AK47Skin.AddItem("Fire Serpent");
	AK47Skin.AddItem("Cartel");
	AK47Skin.AddItem("Emerald Pinstripe");
	AK47Skin.AddItem("Blue Laminate");
	AK47Skin.AddItem("Redline");
	AK47Skin.AddItem("Vulcan");
	AK47Skin.AddItem("Jaguar");
	AK47Skin.AddItem("Jet Set");
	AK47Skin.AddItem("Wasteland Rebel");
	AK47Skin.AddItem("Elite Build");
	AK47Skin.AddItem("Hydroponic");
	AK47Skin.AddItem("Aquamarine Revenge");
	AK47Skin.AddItem("Frontside Misty");
	AK47Skin.AddItem("Point Disarray");
	AK47Skin.AddItem("Fuel Injector");
	AK47Skin.AddItem("Neon Revolution");
	Riflegroup.PlaceLabledControl("AK-47", this, &AK47Skin);

	M41SSkin.SetFileId("m4a1s_skin");
	M41SSkin.AddItem("Dark Water");
	M41SSkin.AddItem("Hyper Beast");
	M41SSkin.AddItem("Boreal Forest");
	M41SSkin.AddItem("VariCamo");
	M41SSkin.AddItem("Nitro");
	M41SSkin.AddItem("Bright Water");
	M41SSkin.AddItem("Atomic Alloy");
	M41SSkin.AddItem("Blood Tiger");
	M41SSkin.AddItem("Guardian");
	M41SSkin.AddItem("Master Piece");
	M41SSkin.AddItem("Knight");
	M41SSkin.AddItem("Cyrex");
	M41SSkin.AddItem("Basilisk");
	M41SSkin.AddItem("Icarus Fell");
	M41SSkin.AddItem("Hot Rod");
	M41SSkin.AddItem("Golden Coi");
	M41SSkin.AddItem("Chantico's Fire");
	M41SSkin.AddItem("Mecha Industries");
	M41SSkin.AddItem("Flashback");
	Riflegroup.PlaceLabledControl("M4A1-S", this, &M41SSkin);

	M4A4Skin.SetFileId("m4a4_skin");
	M4A4Skin.AddItem("Bullet Rain");
	M4A4Skin.AddItem("Zirka");
	M4A4Skin.AddItem("Asiimov");
	M4A4Skin.AddItem("Howl");
	M4A4Skin.AddItem("X-Ray");
	M4A4Skin.AddItem("Desert-Strike");
	M4A4Skin.AddItem("Griffin");
	M4A4Skin.AddItem("Dragon King");
	M4A4Skin.AddItem("Poseidon");
	M4A4Skin.AddItem("Daybreak");
	M4A4Skin.AddItem("Evil Daimyo");
	M4A4Skin.AddItem("Royal Paladin");
	M4A4Skin.AddItem("The BattleStar");
	M4A4Skin.AddItem("Desolate Space");
	M4A4Skin.AddItem("Buzz Kill");
	Riflegroup.PlaceLabledControl("M4A4", this, &M4A4Skin);

	AUGSkin.SetFileId("aug_skin");
	AUGSkin.AddItem("Bengal Tiger");
	AUGSkin.AddItem("Hot Rod");
	AUGSkin.AddItem("Chameleon");
	AUGSkin.AddItem("Torque");
	AUGSkin.AddItem("Radiation Hazard");
	AUGSkin.AddItem("Asterion");
	AUGSkin.AddItem("Daedalus");
	AUGSkin.AddItem("Akihabara Accept");
	AUGSkin.AddItem("Ricochet");
	AUGSkin.AddItem("Fleet Flock");
	AUGSkin.AddItem("Syd Mead");
	Riflegroup.PlaceLabledControl("AUG", this, &AUGSkin);

	FAMASSkin.SetFileId("famas_skin");
	FAMASSkin.AddItem("Contrast Spray");
	FAMASSkin.AddItem("Colony");
	FAMASSkin.AddItem("Cyanospatter");
	FAMASSkin.AddItem("Djinn");
	FAMASSkin.AddItem("Afterimage");
	FAMASSkin.AddItem("Doomkitty");
	FAMASSkin.AddItem("Spitfire");
	FAMASSkin.AddItem("Teardown");
	FAMASSkin.AddItem("Hexane");
	FAMASSkin.AddItem("Pulse");
	FAMASSkin.AddItem("Sergeant");
	FAMASSkin.AddItem("Styx");
	FAMASSkin.AddItem("Neural Net");
	FAMASSkin.AddItem("Survivor");
	FAMASSkin.AddItem("Valence");
	FAMASSkin.AddItem("Roll Cage");
	FAMASSkin.AddItem("Mecha Industries");
	Riflegroup.PlaceLabledControl("FAMAS", this, &FAMASSkin);

	GALILSkin.SetFileId("galil_skin");
	GALILSkin.AddItem("Forest DDPAT");
	GALILSkin.AddItem("Contrast Spray");
	GALILSkin.AddItem("Orange DDPAT");
	GALILSkin.AddItem("Eco");
	GALILSkin.AddItem("Winter Forest");
	GALILSkin.AddItem("Sage Spray");
	GALILSkin.AddItem("VariCamo");
	GALILSkin.AddItem("VariCamo");
	GALILSkin.AddItem("Chatterbox");
	GALILSkin.AddItem("Shattered");
	GALILSkin.AddItem("Kami");
	GALILSkin.AddItem("Blue Titanium");
	GALILSkin.AddItem("Urban Rubble");
	GALILSkin.AddItem("Hunting Blind");
	GALILSkin.AddItem("Sandstorm");
	GALILSkin.AddItem("Tuxedo");
	GALILSkin.AddItem("Cerberus");
	GALILSkin.AddItem("Aqua Terrace");
	GALILSkin.AddItem("Rocket Pop");
	GALILSkin.AddItem("Stone Cold");
	GALILSkin.AddItem("Firefight");
	Riflegroup.PlaceLabledControl("GALIL", this, &GALILSkin);

	SG553Skin.SetFileId("sg552_skin");
	SG553Skin.AddItem("Bulldozer");
	SG553Skin.AddItem("Ultraviolet");
	SG553Skin.AddItem("Damascus Steel");
	SG553Skin.AddItem("Fallout Warning");
	SG553Skin.AddItem("Damascus Steel");
	SG553Skin.AddItem("Pulse");
	SG553Skin.AddItem("Army Sheen");
	SG553Skin.AddItem("Traveler");
	SG553Skin.AddItem("Fallout Warning");
	SG553Skin.AddItem("Cyrex");
	SG553Skin.AddItem("Tiger Moth");
	SG553Skin.AddItem("Atlas");
	Riflegroup.PlaceLabledControl("SG552", this, &SG553Skin);
#pragma endregion

#pragma region MPs
	MPGroup.SetPosition(16, 475);
	MPGroup.SetText("MPs");
	MPGroup.SetSize(376, 165);
	RegisterControl(&MPGroup);

	MAC10Skin.SetFileId("mac10_skin");
	MAC10Skin.AddItem("Tornado");
	MAC10Skin.AddItem("Candy Apple");
	MAC10Skin.AddItem("Silver");
	MAC10Skin.AddItem("Forest DDPAT");
	MAC10Skin.AddItem("Urban DDPAT");
	MAC10Skin.AddItem("Fade");
	MAC10Skin.AddItem("Neon Rider");
	MAC10Skin.AddItem("Ultraviolet");
	MAC10Skin.AddItem("Palm");
	MAC10Skin.AddItem("Graven");
	MAC10Skin.AddItem("Tatter");
	MAC10Skin.AddItem("Amber Fade");
	MAC10Skin.AddItem("Heat");
	MAC10Skin.AddItem("Curse");
	MAC10Skin.AddItem("Indigo");
	MAC10Skin.AddItem("Commuter");
	MAC10Skin.AddItem("Nuclear Garden");
	MAC10Skin.AddItem("Malachite");
	MAC10Skin.AddItem("Rangeen");
	MAC10Skin.AddItem("Lapis Gator");
	MPGroup.PlaceLabledControl("MAC-10", this, &MAC10Skin);

	P90Skin.SetFileId("p90_skin");
	P90Skin.AddItem("Leather");
	P90Skin.AddItem("Virus");
	P90Skin.AddItem("Contrast Spray");
	P90Skin.AddItem("Storm");
	P90Skin.AddItem("Cold Blooded");
	P90Skin.AddItem("Glacier Mesh");
	P90Skin.AddItem("Sand Spray");
	P90Skin.AddItem("Death by Kitty");
	P90Skin.AddItem("Ash Wood");
	P90Skin.AddItem("Fallout Warning");
	P90Skin.AddItem("Scorched");
	P90Skin.AddItem("Emerald Dragon");
	P90Skin.AddItem("Teardown");
	P90Skin.AddItem("Blind Spot");
	P90Skin.AddItem("Trigon");
	P90Skin.AddItem("Desert Warfare");
	P90Skin.AddItem("Module");
	P90Skin.AddItem("Asiimov");
	P90Skin.AddItem("Elite Build");
	P90Skin.AddItem("Shapewood");
	P90Skin.AddItem("Shallow Grave");
	MPGroup.PlaceLabledControl("P90", this, &P90Skin);

	UMP45Skin.SetFileId("ump45_skin");
	UMP45Skin.AddItem("Blaze");
	UMP45Skin.AddItem("Forest DDPAT");
	UMP45Skin.AddItem("Gunsmoke");
	UMP45Skin.AddItem("Urban DDPAT");
	UMP45Skin.AddItem("Grand Prix");
	UMP45Skin.AddItem("Carbon Fiber");
	UMP45Skin.AddItem("Caramel");
	UMP45Skin.AddItem("Fallout Warning");
	UMP45Skin.AddItem("Scorched");
	UMP45Skin.AddItem("Bone Pile");
	UMP45Skin.AddItem("Delusion");
	UMP45Skin.AddItem("Corporal");
	UMP45Skin.AddItem("Indigo");
	UMP45Skin.AddItem("Labyrinth");
	UMP45Skin.AddItem("Minotaur's Labyrinth");
	UMP45Skin.AddItem("Riot");
	UMP45Skin.AddItem("Primal Saber");
	MPGroup.PlaceLabledControl("UMP-45", this, &UMP45Skin);

	BIZONSkin.SetFileId("bizon_skin");
	BIZONSkin.AddItem("Blue Streak");
	BIZONSkin.AddItem("Modern Hunter");
	BIZONSkin.AddItem("Forest Leaves");
	BIZONSkin.AddItem("Bone Mask");
	BIZONSkin.AddItem("Carbon Fiber");
	BIZONSkin.AddItem("Sand Dashed");
	BIZONSkin.AddItem("Urban Dashed");
	BIZONSkin.AddItem("Brass");
	BIZONSkin.AddItem("VariCamo");
	BIZONSkin.AddItem("Irradiated Alert");
	BIZONSkin.AddItem("Rust Coat");
	BIZONSkin.AddItem("Water Sigil");
	BIZONSkin.AddItem("Night Ops");
	BIZONSkin.AddItem("Cobalt Halftone");
	BIZONSkin.AddItem("Antique");
	BIZONSkin.AddItem("Rust Coat");
	BIZONSkin.AddItem("Osiris");
	BIZONSkin.AddItem("Chemical Green");
	BIZONSkin.AddItem("Bamboo Print");
	BIZONSkin.AddItem("Bamboo Forest");
	BIZONSkin.AddItem("Fuel Rod");
	BIZONSkin.AddItem("Photic Zone");
	BIZONSkin.AddItem("Judgement of Anubis");
	MPGroup.PlaceLabledControl("PP-Bizon", this, &BIZONSkin);

	MP7Skin.SetFileId("mp7_skin");
	MP7Skin.AddItem("Groundwater");
	MP7Skin.AddItem("Whiteout");
	MP7Skin.AddItem("Forest DDPAT");
	MP7Skin.AddItem("Anodized Navy");
	MP7Skin.AddItem("Skulls");
	MP7Skin.AddItem("Gunsmoke");
	MP7Skin.AddItem("Contrast Spray");
	MP7Skin.AddItem("Bone Mask");
	MP7Skin.AddItem("Ossified");
	MP7Skin.AddItem("Orange Peel");
	MP7Skin.AddItem("VariCamo");
	MP7Skin.AddItem("Army Recon");
	MP7Skin.AddItem("Groundwater");
	MP7Skin.AddItem("Ocean Foam");
	MP7Skin.AddItem("Full Stop");
	MP7Skin.AddItem("Urban Hazard");
	MP7Skin.AddItem("Olive Plaid");
	MP7Skin.AddItem("Armor Core");
	MP7Skin.AddItem("Asterion");
	MP7Skin.AddItem("Nemesis");
	MP7Skin.AddItem("Special Delivery");
	MP7Skin.AddItem("Impire");
	MPGroup.PlaceLabledControl("MP7", this, &MP7Skin);

	MP9Skin.SetFileId("mp9_skin");
	MP9Skin.AddItem("Ruby Poison Dart");
	MP9Skin.AddItem("Bone Mask");
	MP9Skin.AddItem("Hot Rod");
	MP9Skin.AddItem("Storm");
	MP9Skin.AddItem("Bulldozer");
	MP9Skin.AddItem("Hypnotic");
	MP9Skin.AddItem("Sand Dashed");
	MP9Skin.AddItem("Orange Peel");
	MP9Skin.AddItem("Dry Season");
	MP9Skin.AddItem("Dark Age");
	MP9Skin.AddItem("Rose Iron");
	MP9Skin.AddItem("Green Plaid");
	MP9Skin.AddItem("Setting Sun");
	MP9Skin.AddItem("Dart");
	MP9Skin.AddItem("Deadly Poison");
	MP9Skin.AddItem("Pandora's Box");
	MP9Skin.AddItem("Bioleak");
	MP9Skin.AddItem("Airlock");
	MPGroup.PlaceLabledControl("MP9", this, &MP9Skin);

#pragma endregion

#pragma region Pistols
	PistolGroup.SetPosition(408, 270);
	PistolGroup.SetText("Pistols");
	PistolGroup.SetSize(360, 215);
	RegisterControl(&PistolGroup);

	GLOCKSkin.SetFileId("glock_skin");
	GLOCKSkin.AddItem("Groundwater");
	GLOCKSkin.AddItem("Candy Apple");
	GLOCKSkin.AddItem("Fade");
	GLOCKSkin.AddItem("Night");
	GLOCKSkin.AddItem("Dragon Tattoo");
	GLOCKSkin.AddItem("Twilight Galaxy");
	GLOCKSkin.AddItem("Sand Dune");
	GLOCKSkin.AddItem("Brass");
	GLOCKSkin.AddItem("Catacombs");
	GLOCKSkin.AddItem("Sand Dune");
	GLOCKSkin.AddItem("Steel Disruption");
	GLOCKSkin.AddItem("Blue Fissure");
	GLOCKSkin.AddItem("Death Rattle");
	GLOCKSkin.AddItem("Water Elemental");
	GLOCKSkin.AddItem("Reactor");
	GLOCKSkin.AddItem("Grinder");
	GLOCKSkin.AddItem("Bunsen Burner");
	GLOCKSkin.AddItem("Wraith");
	GLOCKSkin.AddItem("Royal Legion");
	GLOCKSkin.AddItem("Weasel");
	GLOCKSkin.AddItem("Wasteland Rebel");
	PistolGroup.PlaceLabledControl("Glock", this, &GLOCKSkin);

	USPSSkin.SetFileId("usps_skin");
	USPSSkin.AddItem("Forest Leaves");
	USPSSkin.AddItem("Dark Water");
	USPSSkin.AddItem("VariCamo");
	USPSSkin.AddItem("Overgrowth");
	USPSSkin.AddItem("Caiman");
	USPSSkin.AddItem("Blood Tiger");
	USPSSkin.AddItem("Serum");
	USPSSkin.AddItem("Night Ops");
	USPSSkin.AddItem("Stainless");
	USPSSkin.AddItem("Guardian");
	USPSSkin.AddItem("Orion");
	USPSSkin.AddItem("Road Rash");
	USPSSkin.AddItem("Royal Blue");
	USPSSkin.AddItem("Business Class");
	USPSSkin.AddItem("Para Green");
	USPSSkin.AddItem("Torque");
	USPSSkin.AddItem("Kill Confirmed");
	USPSSkin.AddItem("Lead Conduit");
	USPSSkin.AddItem("Cyrex");
	PistolGroup.PlaceLabledControl("USP-S", this, &USPSSkin);

	DEAGLESkin.SetFileId("deagle_skin");
	DEAGLESkin.AddItem("Blaze");
	DEAGLESkin.AddItem("Pilot");
	DEAGLESkin.AddItem("Midnight Storm");
	DEAGLESkin.AddItem("Sunset Storm");
	DEAGLESkin.AddItem("Forest DDPAT");
	DEAGLESkin.AddItem("Crimson Web");
	DEAGLESkin.AddItem("Urban DDPAT");
	DEAGLESkin.AddItem("Night");
	DEAGLESkin.AddItem("Hypnotic");
	DEAGLESkin.AddItem("Mudder");
	DEAGLESkin.AddItem("VariCamo");
	DEAGLESkin.AddItem("Golden Koi");
	DEAGLESkin.AddItem("Cobalt Disruption");
	DEAGLESkin.AddItem("Urban Rubble");
	DEAGLESkin.AddItem("Naga");
	DEAGLESkin.AddItem("Hand Cannon");
	DEAGLESkin.AddItem("Heirloom");
	DEAGLESkin.AddItem("Meteorite");
	DEAGLESkin.AddItem("Conspiracy");
	DEAGLESkin.AddItem("Bronze Deco");
	DEAGLESkin.AddItem("Sunset Storm");
	DEAGLESkin.AddItem("Corinthian");
	DEAGLESkin.AddItem("Kumicho Dragon");
	PistolGroup.PlaceLabledControl("Deagle", this, &DEAGLESkin);


	// Case 2
	DUALSSkin.SetFileId("duals_skin");
	DUALSSkin.AddItem("Anodized Navy");
	DUALSSkin.AddItem("Ossified");
	DUALSSkin.AddItem("Stained");
	DUALSSkin.AddItem("Contractor");
	DUALSSkin.AddItem("Colony");
	DUALSSkin.AddItem("Demolition");
	DUALSSkin.AddItem("Dualing Dragons");
	DUALSSkin.AddItem("Black Limba");
	DUALSSkin.AddItem("Red Quartz");
	DUALSSkin.AddItem("Cobalt Quartz");
	DUALSSkin.AddItem("Hemoglobin");
	DUALSSkin.AddItem("Urban Shock");
	DUALSSkin.AddItem("Marina");
	DUALSSkin.AddItem("Panther");
	DUALSSkin.AddItem("Retribution");
	DUALSSkin.AddItem("Briar");
	DUALSSkin.AddItem("Duelist");
	DUALSSkin.AddItem("Moon in Libra");
	DUALSSkin.AddItem("Cartel");
	DUALSSkin.AddItem("Ventilators");
	DUALSSkin.AddItem("Royal Consorts");
	PistolGroup.PlaceLabledControl("Duals", this, &DUALSSkin);

	FIVESEVENSkin.SetFileId("fiveseven_skin");
	FIVESEVENSkin.AddItem("Candy Apple");
	FIVESEVENSkin.AddItem("Bone Mask");
	FIVESEVENSkin.AddItem("Case Hardened");
	FIVESEVENSkin.AddItem("Contractor");
	FIVESEVENSkin.AddItem("Forest Night");
	FIVESEVENSkin.AddItem("Orange Peel");
	FIVESEVENSkin.AddItem("Jungle");
	FIVESEVENSkin.AddItem("Nitro");
	FIVESEVENSkin.AddItem("Red Quartz");
	FIVESEVENSkin.AddItem("Anodized Gunmetal");
	FIVESEVENSkin.AddItem("Nightshade");
	FIVESEVENSkin.AddItem("Silver Quartz");
	FIVESEVENSkin.AddItem("Kami");
	FIVESEVENSkin.AddItem("Copper Galaxy");
	FIVESEVENSkin.AddItem("Neon Kimono");
	FIVESEVENSkin.AddItem("Fowl Play");
	FIVESEVENSkin.AddItem("Hot Shot");
	FIVESEVENSkin.AddItem("Urban Hazard");
	FIVESEVENSkin.AddItem("Monkey Business");
	FIVESEVENSkin.AddItem("Retrobution");
	FIVESEVENSkin.AddItem("Triumvirate");
	PistolGroup.PlaceLabledControl("Five-Seven", this, &FIVESEVENSkin);

	TECNINESkin.SetFileId("tec9_skin");
	TECNINESkin.AddItem("Tornado");
	TECNINESkin.AddItem("Groundwater");
	TECNINESkin.AddItem("Forest DDPAT");
	TECNINESkin.AddItem("Terrace");
	TECNINESkin.AddItem("Urban DDPAT");
	TECNINESkin.AddItem("Ossified");
	TECNINESkin.AddItem("Hades");
	TECNINESkin.AddItem("Brass");
	TECNINESkin.AddItem("VariCamo");
	TECNINESkin.AddItem("Nuclear Threat");
	TECNINESkin.AddItem("Red Quartz");
	TECNINESkin.AddItem("Tornado");
	TECNINESkin.AddItem("Blue Titanium");
	TECNINESkin.AddItem("Army Mesh");
	TECNINESkin.AddItem("Titanium Bit");
	TECNINESkin.AddItem("Sandstorm");
	TECNINESkin.AddItem("Isaac");
	TECNINESkin.AddItem("Toxic");
	TECNINESkin.AddItem("Bamboo Forest");
	TECNINESkin.AddItem("Avalanche");
	TECNINESkin.AddItem("Jambiya");
	TECNINESkin.AddItem("Re-Entry");
	TECNINESkin.AddItem("Fuel Injector");
	PistolGroup.PlaceLabledControl("Tec-9", this, &TECNINESkin);

	P2000Skin.SetFileId("p2000_skin");
	P2000Skin.AddItem("Grassland Leaves");
	P2000Skin.AddItem("Silver");
	P2000Skin.AddItem("Granite Marbleized");
	P2000Skin.AddItem("Forest Leaves");
	P2000Skin.AddItem("Ossified");
	P2000Skin.AddItem("Handgun");
	P2000Skin.AddItem("Fade");
	P2000Skin.AddItem("Scorpion");
	P2000Skin.AddItem("Grassland");
	P2000Skin.AddItem("Corticera");
	P2000Skin.AddItem("Ocean Foam");
	P2000Skin.AddItem("Pulse");
	P2000Skin.AddItem("Amber Fade");
	P2000Skin.AddItem("Red FragCam");
	P2000Skin.AddItem("Chainmail");
	P2000Skin.AddItem("Coach Class");
	P2000Skin.AddItem("Ivory");
	P2000Skin.AddItem("Fire Elemental");
	P2000Skin.AddItem("Asterion");
	P2000Skin.AddItem("Pathfinder");
	P2000Skin.AddItem("Imperial");
	P2000Skin.AddItem("Oceanic");
	P2000Skin.AddItem("Imperial Dragon");
	PistolGroup.PlaceLabledControl("P2000", this, &P2000Skin);

	P250Skin.SetFileId("p250_skin");
	P250Skin.AddItem("Whiteout");
	P250Skin.AddItem("Metallic DDPAT");
	P250Skin.AddItem("Splash");
	P250Skin.AddItem("Gunsmoke");
	P250Skin.AddItem("Modern Hunter");
	P250Skin.AddItem("Bone Mask");
	P250Skin.AddItem("Boreal Forest");
	P250Skin.AddItem("Sand Dune");
	P250Skin.AddItem("Nuclear Threat");
	P250Skin.AddItem("Mehndi");
	P250Skin.AddItem("Facets");
	P250Skin.AddItem("Hive");
	P250Skin.AddItem("Muertos");
	P250Skin.AddItem("Steel Disruption");
	P250Skin.AddItem("Undertow");
	P250Skin.AddItem("Franklin");
	P250Skin.AddItem("Neon Kimono");
	P250Skin.AddItem("Supernova");
	P250Skin.AddItem("Contamination");
	P250Skin.AddItem("Cartel");
	P250Skin.AddItem("Valence");
	P250Skin.AddItem("Crimson Kimono");
	P250Skin.AddItem("Mint Kimono");
	P250Skin.AddItem("Wing Shot");
	P250Skin.AddItem("Asiimov");
	PistolGroup.PlaceLabledControl("P250", this, &P250Skin);

#pragma endregion
}

/*void CPlayersTab::Setup()
{
SetTitle("PlayerList");

#pragma region PList

pListGroup.SetPosition(16, 16);
pListGroup.SetSize(680, 200);
pListGroup.SetText("Player List");
pListGroup.SetColumns(2);
RegisterControl(&pListGroup);

pListPlayers.SetPosition(26, 46);
pListPlayers.SetSize(640, 50);
pListPlayers.SetHeightInItems(20);
RegisterControl(&pListPlayers);

#pragma endregion

#pragma region Options

OptionsGroup.SetPosition(16, 257);
OptionsGroup.SetSize(450, 120);
OptionsGroup.SetText("Player Options");
RegisterControl(&OptionsGroup);

OptionsFriendly.SetFileId("pl_friendly");
OptionsGroup.PlaceLabledControl("Friendly", this, &OptionsFriendly);

OptionsAimPrio.SetFileId("pl_priority");
OptionsGroup.PlaceLabledControl("Priority", this, &OptionsAimPrio);

OptionsCalloutSpam.SetFileId("pl_callout");
OptionsGroup.PlaceLabledControl("Callout Spam", this, &OptionsCalloutSpam);

#pragma endregion
}

DWORD GetPlayerListIndex(int EntId)
{
player_info_t pinfo;
Interfaces::Engine->GetPlayerInfo(EntId, &pinfo);

// Bot
if (pinfo.guid[0] == 'B' && pinfo.guid[1] == 'O')
{
char buf[64]; sprintf_s(buf, "BOT_420%sAY", pinfo.name);
return CRC32(buf, 64);
}
else
{
return CRC32(pinfo.guid, 32);
}
}

bool IsFriendly(int EntId)
{
DWORD plistId = GetPlayerListIndex(EntId);
if (PlayerList.find(plistId) != PlayerList.end())
{
return PlayerList[plistId].Friendly;
}

return false;
}

bool IsAimPrio(int EntId)
{
DWORD plistId = GetPlayerListIndex(EntId);
if (PlayerList.find(plistId) != PlayerList.end())
{
return PlayerList[plistId].AimPrio;
}

return false;
}

bool IsCalloutTarget(int EntId)
{
DWORD plistId = GetPlayerListIndex(EntId);
if (PlayerList.find(plistId) != PlayerList.end())
{
return PlayerList[plistId].Callout;
}

return false;
}

void UpdatePlayerList()
{
IClientEntity* pLocal = Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());
if (Interfaces::Engine->IsConnected() && Interfaces::Engine->IsInGame() && pLocal && pLocal->IsAlive())
{
Menu::Window.Playerlist.pListPlayers.ClearItems();

// Loop through all active entitys
for (int i = 0; i < Interfaces::EntList->GetHighestEntityIndex(); i++)
{
// Get the entity

player_info_t pinfo;
if (i != Interfaces::Engine->GetLocalPlayer() && Interfaces::Engine->GetPlayerInfo(i, &pinfo))
{
IClientEntity* pEntity = Interfaces::EntList->GetClientEntity(i);
int HP = 100; char* Location = "Unknown";
char *Friendly = " ", *AimPrio = " ";

DWORD plistId = GetPlayerListIndex(Menu::Window.Playerlist.pListPlayers.GetValue());
if (PlayerList.find(plistId) != PlayerList.end())
{
Friendly = PlayerList[plistId].Friendly ? "Friendly" : "";
AimPrio = PlayerList[plistId].AimPrio ? "AimPrio" : "";
}

if (pEntity && !pEntity->IsDormant())
{
HP = pEntity->GetHealth();
Location = pEntity->GetLastPlaceName();
}

char nameBuffer[512];
sprintf_s(nameBuffer, "%-24s %-10s %-10s [%d HP] [Last Seen At %s]", pinfo.name, IsFriendly(i) ? "Friend" : " ", IsAimPrio(i) ? "AimPrio" : " ", HP, Location);
Menu::Window.Playerlist.pListPlayers.AddItem(nameBuffer, i);

}

}

DWORD meme = GetPlayerListIndex(Menu::Window.Playerlist.pListPlayers.GetValue());

// Have we switched to a different player?
static int PrevSelectedPlayer = 0;
if (PrevSelectedPlayer != Menu::Window.Playerlist.pListPlayers.GetValue())
{
if (PlayerList.find(meme) != PlayerList.end())
{
Menu::Window.Playerlist.OptionsFriendly.SetState(PlayerList[meme].Friendly);
Menu::Window.Playerlist.OptionsAimPrio.SetState(PlayerList[meme].AimPrio);
Menu::Window.Playerlist.OptionsCalloutSpam.SetState(PlayerList[meme].Callout);

}
else
{
Menu::Window.Playerlist.OptionsFriendly.SetState(false);
Menu::Window.Playerlist.OptionsAimPrio.SetState(false);
Menu::Window.Playerlist.OptionsCalloutSpam.SetState(false);

}
}
PrevSelectedPlayer = Menu::Window.Playerlist.pListPlayers.GetValue();

PlayerList[meme].Friendly = Menu::Window.Playerlist.OptionsFriendly.GetState();
PlayerList[meme].AimPrio = Menu::Window.Playerlist.OptionsAimPrio.GetState();
PlayerList[meme].Callout = Menu::Window.Playerlist.OptionsCalloutSpam.GetState();
}
}*/

void Menu::SetupMenu()
{
	Window.Setup();

	GUI.RegisterWindow(&Window);
	GUI.BindWindow(VK_INSERT, &Window);
}

void Menu::DoUIFrame()
{
	// General Processing

	// If the "all filter is selected tick all the others
	if (Window.VisualsTab.FiltersAll.GetState())
	{
		Window.VisualsTab.FiltersC4.SetState(true);
		Window.VisualsTab.FiltersChickens.SetState(true);
		Window.VisualsTab.FiltersPlayers.SetState(true);
		Window.VisualsTab.FiltersWeapons.SetState(true);
	}

	GUI.Update();
	GUI.Draw();


}


