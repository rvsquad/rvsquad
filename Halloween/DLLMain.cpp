// General shit
#include "DLLMain.h"
#include "Utilities.h"

// Injection stuff
#include "INJ/ReflectiveLoader.h"

// Stuff to initialise
#include "Offsets.h"
#include "Interfaces.h"
#include "Hooks.h"
#include "RenderManager.h"
#include "Hacks.h"
#include "Menu.h"
#include "AntiAntiAim.h"
#include "MiscHacks.h"
#include "Dumping.h"

float lineRealAngle;
float lineFakeAngle;
float headPos;
float flipBool;
int predicting;

// Used as part of the reflective DLL injection
extern HINSTANCE hAppInstance;

// Our DLL Instance
HINSTANCE HThisModule;
bool DoUnload;

UCHAR
szFileSys[255],
szVolNameBuff[255];

DWORD
dwMFL,
dwSysFlags,
dwSerial;

// Our thread we use to setup everything we need
// Everything appart from code in hooks get's called from inside 
// here.

#define rprt (70050651) //Main Coder
#define vilnar (1213810804) //Member-SecondCoder

VOID InitWithHWID()
{		
	GetVolumeInformation("C:\\", (LPTSTR)szVolNameBuff, 255, &dwSerial, &dwMFL, &dwSysFlags, (LPTSTR)szFileSys, 255);
	if (dwSerial == rprt || dwSerial == vilnar)
	{
		MessageBox(0, "have fun", "Counter-Strike: Global Offensive", MB_OK | MB_ICONINFORMATION);
		Offsets::Initialise(); // <--- Checking offsets and setting up
		Interfaces::Initialise(); // <--- Getting pointers to the sdk files
		NetVar.RetrieveClasses(); // <--- Setting up netvars
		Render::Initialise(); // <--- Render engine setting up
		JDGHYUEHRGYUWEFGTYQAFTYFR6T1::SetupHacks(); // <--- Loading functions
		Menu::SetupMenu(); // <--- Setting up the menu
		Hooks::Initialise(); // <--- Using glue to make it work lmao
		ApplyAAAHooks(); // <--- AAA Shit starting
		ApplyNetVarsHooks(); // <--- Applying NetVars
		Dump::DumpClassIds(); // <--- Dumping classes
	}
	else
	{
		MessageBox(0, "wrong hwid", "Counter-Strike: Global Offensive", MB_OK | MB_ICONERROR);
	}
}

// DllMain
// Entry point for our module
BOOL WINAPI DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD     fdwReason,
	_In_ LPVOID    lpvReserved
	)
{
	switch (fdwReason)
	{
	case DLL_QUERY_HMODULE:
		if (lpvReserved != NULL)
			*(HMODULE *)lpvReserved = hAppInstance;
		break;
	case DLL_PROCESS_ATTACH:
		HThisModule = hinstDLL;
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)InitWithHWID, NULL, NULL, NULL);
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}